# Campaign-Manager Microservice
## Version 2.0.3.0
## Requirements: WEB-5673
## Name: cmmsDistributionListProcessing 
## Symfony version:  symfony new cmmsDistributionListProcessing --version=5.4.1
## PHP Version: 8.0

## CAUTION: 

to run that service as "symfony server:start" on your local machine, you must switch to PHP 8.0!! Tipp: install a ubuntu-desktop as VM, clone the repo there & install PHPStorm, since it's easy to install and switch PHP versions in ubuntu.

### composer.json => change _"php": ">=7.2.5"_   to  _"php": ">=8.0"_ !!!
     {

        "type": "project",

        "license": "proprietary",

        "minimum-stability": "stable",

        "prefer-stable": true,

        "require": {

            "php": ">=7.2.5",   <<<<< change !!!!!
        ...


### composer install
if composer install throws errors, check 

a) for correct php version (8.0) and/or 

b) run composer update after composer install 



## Description

CM MicroService for distributing campaign-manager **Email Distribution List**  ("_Verteilerlisten_") files (*.csv) to connected Email-Marketing-Systems ("_EMS_" respectively "_ASP_" in CM) of a corresponding tenant ("_Mandant_" in CM)
   - **Maileon**
   - (_Promio_ => add later),
   - (_Sendeffect_ add later),
   - (_Empaction_ add later) 
   - and so on ...

## Primary Concept

takes an imported csv-file and provides via REST-API 
- **PREPROCESSING**:
  - **determines**, **counts** and **flags (=> add status)** for each active and connected Email-Marketing-System (e.g. "_Stellar Basis, FM2021, fulle.media Basis, ..._") of corresponding tenant (e.g. "_fulle.media_")
      - _Brutto, 	Netto, 	Quote (calc), 	EXT (???), 	Bereits erhalten, 	Dubletten, 	Intradubletten, 	Blacklist_
      - brutto count of email-records of received import-csv-file of some delivering party
      - netto count of email-records (_??? how to count => **new emails**  ???_) 
      - blacklisted emails (_??? should hashes be checked => which ones ???_)
      - Duplicates (already present emails in contact-list with DOI)
      - "Intra"-Duplicates 
      - **new emails**, which where neither blacklisted, unsubscribed nor are present in contact list with DOI in corresponding Email-Marketing-System
  - add additional columns (e.g. **deliver-ID**) with defined values (e.g. deliver-ID's **value**  is an autoincrement column, starting from 800.000, per record and input-csv-file)
    - list of all columns for Stellar Group
      - ...
    - list of all columns for Cooper-Group
      - ...
- **DISTRIBUTION**
  - adds only **new emails** of an import-csv-file to corresponding Email-Marketing-System (e.g. _Stellar Basis_) contact list by using (Maileon) configured SFTP-Server of that Email-Marketing-System (e.g. _Stellar Basis_)
- **REPORTING / LOGGING / DATA-SAVING**
  - enters in corresponding CM database table (`add tablename`) below PREPROCESSING mentioned counts, **reporting** via CM-UI
  - logs preprocessing status and distribution status in new, to be created table for **reporting purposes** (_??? access via MIO or currently still from CM-UI ???_)
    - **stores** each preprocessed record with status and timestamp, assigned delivery-origin ("_Lieferant_"), timestamp of creation, timestamp of preprocessing, timestamp of distribution, list of distribution targets (connected Email-Marketing-System as e.g. "Stellar Basis")


# How it works
... to be described ... (but currently there is no time for that)


## Uses Databases
- peppmt@62.138.184.191
- <mandantDb**>@62.138.184.191

<mandantDb**>: all mandant specific databases

## Currently attached EM-Systems (EMS)
- Maileon - 2021-12
- ...

## Documentation
https://treaction.atlassian.net/wiki/spaces/WEB/pages/633045002/Neuen+Mandant+im+Campaign+Manager+aufbauen


## STATS  2022-03-01

###Size
Lines of Code (LOC)                             3245

Comment Lines of Code (CLOC)                     840 (25.89%)

Non-Comment Lines of Code (NCLOC)               2405 (74.11%)

Logical Lines of Code (LLOC)                     878 (27.06%)

Classes                                        878 (100.00%)

Average Class Length                          67

Minimum Class Length                         0

Maximum Class Length                       453

Average Method Length                          7

Minimum Method Length                        0

Maximum Method Length                       63

Average Methods Per Class                      8

Minimum Methods Per Class                    0

Maximum Methods Per Class                   31

Functions                                        0 (0.00%)

Average Function Length                        0

Not in classes or functions                      0 (0.00%)

###Cyclomatic Complexity

Average Complexity per LLOC                     0.28

Average Complexity per Class                   20.00

Minimum Class Complexity                      1.00

Maximum Class Complexity                    144.00

Average Complexity per Method                   3.30

Minimum Method Complexity                     1.00

Maximum Method Complexity                    25.00

###Dependencies

Global Accesses                                    5

Global Constants                                 0 (0.00%)

Global Variables                                 0 (0.00%)

Super-Global Variables                           5 (100.00%)

Attribute Accesses                               311

Non-Static                                     311 (100.00%)

Static                                           0 (0.00%)

Method Calls                                     488

Non-Static                                     486 (99.59%)

Static                                           2 (0.41%)

###Structure

Namespaces                                         7

Interfaces                                         1

Traits                                             1

Classes                                           11

Abstract Classes                                 0 (0.00%)

Concrete Classes                                11 (100.00%)

Final Classes                                  0 (0.00%)

Non-Final Classes                             11 (100.00%)

Methods                                          109

###Scope
Non-Static Methods                           108 (99.08%)

Static Methods                                 1 (0.92%)

###Visibility
Public Methods                                74 (67.89%)

Protected Methods                              1 (0.92%)

Private Methods                               34 (31.19%)

Functions                                          0

Named Functions                                  0 (0.00%)

Anonymous Functions                              0 (0.00%)

Constants                                          8

Global Constants                                 0 (0.00%)

Class Constants                                  8 (100.00%)

Public Constants                               0 (0.00%)

Non-Public Constants                           8 (100.00%)`


# SQL: create table ftp_upload_planer in database peppmt
````

create table ftp_upload_planer
(
id bigint unsigned auto_increment
primary key,
transaction_id varchar(255) default 'NO ID' not null,
mandant_id int null,
import_nr int(11) unsigned null,
filename varchar(255) null,
path_filename text null,
start_upload_after_datetime datetime null,
planed_by_user_id int null,
processing_status enum('planed', 'preprocessed', 'uploaded') default 'planed' null,
preprocessing_stats text null,
preprocessed_at datetime null,
consolidation_filename varchar(255) null,
uploaded_to_esp text null,
upload_end_datetime datetime null,
created_at datetime null,
updated_at datetime null
)
charset=utf8mb4;

create index idxfilename
on ftp_upload_planer (filename(191));

create index idxmid
on ftp_upload_planer (mandant_id);

create index idxtaid
on ftp_upload_planer (transaction_id(191));

create index processing_status
on ftp_upload_planer (processing_status);
 
