<?php

namespace App\EspServices;

/**
 * dummy, not yet implemented (2022-01)
 */
class PromioService implements EspServiceInterface
{
    public function __construct()
    {
    }

    public function reactivateUnsubscriber(string $email, array $espConnectionParams):array
    {
        // TODO: Implement reactivateUnsubscriber() method.
        return ['status' =>true, 'message'=> 'some message'];
    }

    public function sendToEsp(array $config): ?array
    {
        // TODO: Implement sendToEsp() method.
        return ['status' =>true, 'message'=> 'some message'];
    }

    public function addCustomFields(array $config): ?array
    {
        // TODO: Implement addCustomFields() method.
        return ['status' =>true, 'message'=> 'some message'];
    }
}
