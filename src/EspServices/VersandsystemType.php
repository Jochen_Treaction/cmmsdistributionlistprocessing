<?php

namespace App\EspServices;

/**
 * yes, this is oversized!!!
 */
class VersandsystemType
{
    private string $versandsystem;

    public function __construct(string $versandsystem)
    {
        $this->versandsystem=$versandsystem;
    }


    /**
     * @return string
     */
    public function getVersandsystem(): string
    {
        return $this->versandsystem;
    }


    /**
     * @param string $versandsystem
     */
    public function setVersandsystem(string $versandsystem): void
    {
        $this->versandsystem = $versandsystem;
    }
}
