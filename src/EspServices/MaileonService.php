<?php

namespace App\EspServices;

use App\Helper\LoggerTrait;

class MaileonService implements EspServiceInterface
{
    use LoggerTrait;

    public function __construct() // construct must stay empty: no parameters
    {
    }


    /**
     * @param string $email
     * @param array  $espConnectionParams ['apikey', 'firstname', 'lastname']
     * @return array
     */
    public function reactivateUnsubscriber(string $email, array $espConnectionParams):array
    {
        if(empty($espConnectionParams) || empty($espConnectionParams['apikey']) || empty($espConnectionParams['firstname']) || empty($espConnectionParams['lastname']) || empty($email)) {
            return ['status' => false, 'message' => "espConnectionParams for email $email not correctly set. method=" . __METHOD__ . ', line=' . __LINE__];
        }

        // "standard_fields":{"FIRSTNAME":"Max","LASTNAME":"Mustermann"}
        $jsonPostData = json_encode([
            'standard_fields' => [
                'FIRSTNAME'=>$espConnectionParams['firstname'],
                'LASTNAME'=> $espConnectionParams['lastname'],
            ]
        ]);
        $result = $this->helperMaileonReactivateUnsubscriber($espConnectionParams['apikey'], $email, $jsonPostData);

        if (null === $result) {
            return ['status' => false, 'message' => "could not reactivate $email. method=" . __METHOD__ . ', line=' . __LINE__, 'customFieldStatus' => $customFieldStatus];
        }

        return ['status' => true, 'message' => "reactivated $email from unsubscribers", 'customFieldStatus' => $customFieldStatus];
    }

    /**
     * @param string $apikey
     * @param string $email
     * @param string $jsonPostData
     * @return array|null
     * @see https://maileon.com/support/create-contact-json/
     */
    private function helperMaileonReactivateUnsubscriber(string $apikey, string $email, string $jsonPostData): ?array
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.maileon.com/1.0/contacts/email/$email?permission=4&sync_mode=1&doi=false",
			CURLOPT_SSL_VERIFYPEER => false, // make curl request work from local sys
			CURLOPT_SSL_VERIFYSTATUS => false, // make p curl request work from local sys
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $jsonPostData,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/vnd.maileon.api+json',
                'Authorization: Basic ' . base64_encode($apikey),
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        $this->logInfo('$response', ['curl_response' =>$response, 'curl_error'=>$error, __METHOD__, __LINE__]);

        curl_close($curl);

        $obj = simplexml_load_string($response);

        if (false === $obj) {
            return null;
        }

        return json_decode(json_encode($obj), true, 512, JSON_OBJECT_AS_ARRAY);
    }


    /**
     * @param array $config  for Mailean: needs key 'apikey' => <i>$config['apikey'] = 'esp-apikey'</i>
     * @return array ['customFields' => (array) of all custom fields found in ESP, 'added' => status which custom fields where added]
     */
    public function addCustomFields(array $config):?array
    {
        if(empty($config) || empty($config['apikey'])) {
            return null;
        }

        $customFieldStatus = $this->mainHelperSetCustomFields($config['apikey']);
        return $customFieldStatus;
    }


    /**
     * @param string $apikey
     * @return array  ['customFields' => (array) of all custom fields found in ESP, 'added' => status which custom fields where added]
     */
    private function mainHelperSetCustomFields(string $apikey): array
    {
        $added = [];
        $customFields = $this->subHelperGetCustomFields($apikey);

        foreach(['PartnerID','DeliveryID','HistoryID'] as $customField) {
            $this->logInfo("customField = $customField", [__METHOD__, __LINE__]);
            if (!in_array($customField, $customFields)) {
                $added[$apikey][$customField] = $this->subHelperAddCustomField($customField, $apikey);
            }
        }
        return ['customFields' => $customFields, 'added' => $added];
    }


    /**
     * @param string $apikey
     * @return array|null
     */
    private function subHelperGetCustomFields(string $apikey):?array
    {
        $customFieldsArray=[];
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.maileon.com/1.0/contacts/fields/custom",
            CURLOPT_SSL_VERIFYPEER => false, // make curl request work from local sys
            CURLOPT_SSL_VERIFYSTATUS => false, // make p curl request work from local sys
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/vnd.maileon.api+json',
                'Authorization: Basic ' . base64_encode($apikey),
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);

        curl_close($curl);
        $obj = simplexml_load_string($response);

        if (false === $obj) {
            return null;
        }

        $customFieldsArray = json_decode(json_encode($obj), true, 512, JSON_OBJECT_AS_ARRAY);
        $returnCustomFields = [];

        foreach ($customFieldsArray['custom_field'] as $record) {
            $returnCustomFields[] = $record['name'];
        }

        return $returnCustomFields;
    }

    /**
     * @param string $customField
     * @param string $apikey
     * @param string $type default = 'integer'
     * @return bool
     */
    private function subHelperAddCustomField(string $customField, string $apikey, $type='integer'):bool
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.maileon.com/1.0/contacts/fields/custom/{$customField}?type={$type}",
            CURLOPT_SSL_VERIFYPEER => false, // make curl request work from local sys
            CURLOPT_SSL_VERIFYSTATUS => false, // make p curl request work from local sys
            // CURLOPT_RETURNTRANSFER => true, // only check if curl was successful or not, since endpoint does not return anything
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/vnd.maileon.api+xml',
                'charset: utf-8',
                'Authorization: Basic ' . base64_encode($apikey)
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        $this->logInfo('subHelperAddCustomField', ['error' => $error, 'response' => $response, __METHOD__, __LINE__]);

        curl_close($curl);
        return ($response) ? true: false;
    }


    /**
     * @internal not yet used: TODO: Implement sendToEsp() method as substitution for CsvProcessingService::ftpToEsp
     * @param array $config
     * @return array|null
     */
    public function sendToEsp(array $config): ?array
    {
        // TODO: Implement sendToEsp() method as substitution for CsvProcessingService::ftpToEsp
        return ['status' =>true, 'message'=> 'some message'];
    }
}
