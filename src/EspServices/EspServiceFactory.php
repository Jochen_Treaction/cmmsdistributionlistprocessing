<?php

namespace App\EspServices;

use App\EspServices\EspServiceInterface;
use App\EspServices\MaileonService;
use App\EspServices\PromioService;
use App\EspServices\VersandsystemType;

class EspServiceFactory
{
    public static function createEspService(VersandsystemType $versandsystemType): ?EspServiceInterface
    {
        switch ($versandsystemType->getVersandsystem()) {
            case 'Maileon' :
                $instance = new MaileonService();
                break;
            case 'Promio' :
                $instance = new PromioService();
                break;
            default:
                $instance = null;
                break;
        }
        return $instance;
    }

}
