<?php

namespace App\EspServices;

interface EspServiceInterface
{
    /**
     * @param string $email
     * @param array  $espConnectionParams
     * @return array|null return ['status' =>true|false, 'message'=> 'some message'] status true=success, status false=error
     */
    public function reactivateUnsubscriber(string $email, array $espConnectionParams):?array;

    public function sendToEsp(array $config):?array;

    public function addCustomFields(array $config):?array;

}
