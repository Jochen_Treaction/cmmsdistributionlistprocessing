<?php

namespace App\Services;

/**
 * @internal class to collect processing status information for each
 */
class ProcessingStatusService
{
    /**
     * @var int $countBrutto
     */
    private int $countBrutto;
    /**
     * @var int $countNetto
     */
    private int $countNetto;
    /**
     * @var int $countBlacklistHits
     */
    private int $countBlacklistHits;
    /**
     * @var int $countAbmelderHits
     */
    private int $countAbmelderHits;
    /**
     * @var int $countNonWhiteListHits
     */
    private int $countNonWhiteListHits;
    /**
     * @var int $countInSetDuplicates
     */
    private int $countInSetDuplicates;

    /**
     * @var array $arrayIncompleteRecords
     */
    private array $arrayIncompleteRecords;


    /**
     * @var array $arrayNonWhitelistRecords
     */
    private array $arrayNonWhitelistRecords;

    /**
     * @var int $countEspDuplicates
     */
    private int $countEspDuplicates;

    /**
     * @var int $countExistingLeadsInHistory
     */
    private int $countExistingLeadsInHistory;

    /**
     * @var bool $boolBlacklistHits
     * @deprecated
     */
    private bool $boolBlacklistHits;
    /**
     * @var bool $boolAbmelderHits
     * @deprecated
     */
    private bool $boolAbmelderHits;
    /**
     * @var bool $boolNonWhiteListHits
     * @deprecated
     */
    private bool $boolNonWhiteListHits;


    public function __construct()
    {
        $this->setCountBrutto(0);
        $this->setCountNetto(0);
        $this->setCountBlacklistHits(0);
        $this->setCountAbmelderHits(0);
        $this->setCountEspDuplicates(0);
        $this->setCountInSetDuplicates(0);
        $this->setCountNonWhiteListHits(0);
        $this->setCountExistingLeadsInHistory(0);
        // $this->setBoolBlacklistHits(false);
        // $this->setBoolAbmelderHits(false);
        // $this->setBoolNonWhiteListHits(false);
        $this->arrayIncompleteRecords = [];
        $this->arrayNonWhitelistRecords=[];
    }


    /**
     * @return array
     */
    public function getArrayIncompleteRecords(): array
    {
        return $this->arrayIncompleteRecords;
    }


    /**
     * @param int $arrayIncompleteRecords
     */
    public function setArrayIncompleteRecords(array $arrayIncompleteRecords): void
    {
        $this->arrayIncompleteRecords = $arrayIncompleteRecords;
    }


    /**
     * @return array
     */
    public function getArrayNonWhitelistRecords(): array
    {
        return $this->arrayNonWhitelistRecords;
    }


    /**
     * @param array $arrayNonWhitelistRecords
     */
    public function setArrayNonWhitelistRecords(array $arrayNonWhitelistRecords): void
    {
        $this->arrayNonWhitelistRecords = $arrayNonWhitelistRecords;
    }

    /**
     * @return int
     */
    public function getCountBrutto(): int
    {
        return $this->countBrutto;
    }


    /**
     * @param int $countBrutto
     */
    public function setCountBrutto(int $countBrutto): void
    {
        $this->countBrutto = $countBrutto;
    }


    /**
     * @return int
     */
    public function getCountNetto(): int
    {
        return $this->countNetto;
    }


    /**
     * @param int $countNetto
     */
    public function setCountNetto(int $countNetto): void
    {
        $this->countNetto = $countNetto;
    }


    /**
     * @return int
     */
    public function getCountBlacklistHits(): int
    {
        return $this->countBlacklistHits;
    }


    /**
     * @param int $countBlacklistHits
     */
    public function setCountBlacklistHits(int $countBlacklistHits): void
    {
        $this->countBlacklistHits = $countBlacklistHits;
    }


    /**
     * @return int
     */
    public function getCountAbmelderHits(): int
    {
        return $this->countAbmelderHits;
    }


    /**
     * @param int $countAbmelderHits
     */
    public function setCountAbmelderHits(int $countAbmelderHits): void
    {
        $this->countAbmelderHits = $countAbmelderHits;
    }

    /**
     * @deprecated
     */
    public function incrementCountAbmelderHits(): void
    {
        $this->countAbmelderHits += 1;
    }


    /**
     * @return int
     */
    public function getCountNonWhiteListHits(): int
    {
        return $this->countNonWhiteListHits;
    }


    /**
     * @param int $countNonWhiteListHits
     */
    public function setCountNonWhiteListHits(int $countNonWhiteListHits): void
    {
        $this->countNonWhiteListHits = $countNonWhiteListHits;
    }


    /**
     * @return int
     */
    public function getCountInSetDuplicates(): int
    {
        return $this->countInSetDuplicates;
    }


    /**
     * @param int $countInSetDuplicates
     */
    public function setCountInSetDuplicates(int $countInSetDuplicates): void
    {
        $this->countInSetDuplicates = $countInSetDuplicates;
    }


    /**
     * @return int
     */
    public function getCountEspDuplicates(): int
    {
        return $this->countEspDuplicates;
    }


    /**
     * @param int $countEspDuplicates
     */
    public function setCountEspDuplicates(int $countEspDuplicates): void
    {
        $this->countEspDuplicates = $countEspDuplicates;
    }


    /**
     * @return int
     */
    public function getCountExistingLeadsInHistory(): int
    {
        return $this->countExistingLeadsInHistory;
    }


    /**
     * @param int $countExistingLeadsInHistory
     */
    public function setCountExistingLeadsInHistory(int $countExistingLeadsInHistory): void
    {
        $this->countExistingLeadsInHistory = $countExistingLeadsInHistory;
    }

    /**
     * @deprecated
     */
    public function incrementCountExistingLeadsInHistory(): void
    {
        $this->countExistingLeadsInHistory += 1;
    }


    /**
     * @return bool
     * @deprecated
     */
    public function isBoolBlacklistHits(): bool
    {
        return $this->boolBlacklistHits;
    }


    /**
     * @param bool $boolBlacklistHits
     * @deprecated
     */
    public function setBoolBlacklistHits(bool $boolBlacklistHits): void
    {
        $this->boolBlacklistHits = $boolBlacklistHits;
    }


    /**
     * @return bool
     * @deprecated
     */
    public function isBoolAbmelderHits(): bool
    {
        return $this->boolAbmelderHits;
    }


    /**
     * @param bool $boolAbmelderHits
     * @deprecated
     */
    public function setBoolAbmelderHits(bool $boolAbmelderHits): void
    {
        $this->boolAbmelderHits = $boolAbmelderHits;
    }


    /**
     * @return bool
     * @deprecated
     */
    public function isBoolNonWhiteListHits(): bool
    {
        return $this->boolNonWhiteListHits;
    }


    /**
     * @param bool $boolNonWhiteListHits
     * @deprecated
     */
    public function setBoolNonWhiteListHits(bool $boolNonWhiteListHits): void
    {
        $this->boolNonWhiteListHits = $boolNonWhiteListHits;
    }


    /**
     * @return string json
     */
    public function getJson(): string
    {
        return json_encode([
                'countBrutto' => $this->countBrutto,
                'countNetto' => $this->countNetto,
                'countBlacklistHits' => $this->countBlacklistHits,
                'countAbmelderHits' => $this->countAbmelderHits,
                'countNonWhiteListHits' => $this->countNonWhiteListHits,
                'countInSetDuplicates' => $this->countInSetDuplicates,
                'arrayIncompleteRecordStructure' => count($this->arrayIncompleteRecords),
                'countEspDuplicates' => $this->countEspDuplicates,
            ]);
    }

    /**
     * @return array
     */
    public function getArray(): array
    {
        return [
                'countBrutto' => $this->countBrutto,
                'countNetto' => $this->countNetto,
                'countBlacklistHits' => $this->countBlacklistHits,
                'countAbmelderHits' => $this->countAbmelderHits,
                'countNonWhiteListHits' => $this->countNonWhiteListHits,
                'countInSetDuplicates' => $this->countInSetDuplicates,
                'countExistingLeadsInHistory' => $this->countExistingLeadsInHistory,
                'countEspDuplicates' => $this->countEspDuplicates,
                'countArrayIncompleteRecordStructure' => count($this->arrayIncompleteRecords),
                'arrayIncompleteRecordStructure' => $this->arrayIncompleteRecords,
                'countArrayNonWhitelistRecords' => count($this->arrayNonWhitelistRecords),
                'arrayNonWhitelistRecords' => $this->arrayNonWhitelistRecords,
            ];
    }

}
