<?php

namespace App\Services;

use App\EspServices\VersandsystemType;
use Doctrine\DBAL\Exception;
use Psr\Log\LoggerInterface;
use App\Services\ProcessingStatusService;
use App\Repository\PeppmtRepository;
use App\EspServices\EspServiceFactory;
use App\EspServices\EspServiceInterface;

class CsvProcessingService
{
    private const FTP_PASV_FAILED = -4;
    private const FTP_SSL_CONNECT_FAILED = -3;
    private const FTP_LOGIN_FAILED = -2;
    private const FTP_UPLOAD_FAILED = -1;
    private const FTP_UPLOAD_SUCCESSFULL = 1;
    private const LINE_SEPARATOR_RN = "\r\n"; // use double quotes here
    private const LINE_SEPARATOR_N = "\n"; // use double quotes here
    private const LINE_SEPARATOR_R = "\r"; // use double quotes here

    /**
     * defined as parameter in services.yaml
     * @var string $uploadDir
     */
    private string $uploadDir;
    /**
     * defined as parameter in services.yaml
     * @var string $cmDbUser
     */
    private string $cmDbUser;
    /**
     * defined as parameter in services.yaml
     * @var string $cmDbPassword
     */
    private string $cmDbPassword;
    /**
     * @var \App\Services\ProcessingStatusService $processingStatusService
     */
    private ProcessingStatusService $processingStatusService;
    /**
     * a class related "global" array[mandantId][versandsystemId] = new App\Services\ProcessingStatusService()
     * @var $processStatus
     */
    private array $processStatus;
    /**
     * this array is used for distribution of clean and enriched csv to Maileon (alias XQUEUE alias eMIO) by ftp
     * @var array  [versandsystem_type,  mandant_id, versandsystem_id, versandsystem_asp, versandsystem_asp_abkz, versandsystem_ftpuser, versandsystem_ftppw]
     */
    private array $allActiveEmailDistributionSystems;

    /**
     * @var string $redisHost
     */
    private string $redisHost;
    /**
     * @var string $redisPort
     */
    private string $redisPort;
    /**
     * @var string $redisUser
     */
    private string $redisUser;
    /**
     * @var string $redisPw
     */
    private string $redisPw;



    public function __construct(
        LoggerInterface $logger,
        PeppmtRepository $peppmtRepository,
        ProcessingStatusService $processingStatusService,
        string $uploadDir,
        string $cmDbUser,
        string $cmDbPassword,
        string $redisHost,
        string $redisPort,
        string $redisUser,
        string $redisPw

    ) {
        $this->logger = $logger;
        $this->peppmtRepository = $peppmtRepository;
        $this->uploadDir = $uploadDir;
        $this->cmDbUser = $cmDbUser;
        $this->cmDbPassword = $cmDbPassword;
        $this->redisHost =$redisHost;
        $this->redisPort = $redisPort;
        $this->redisUser =$redisUser;
        $this->redisPw = $redisPw;

        $this->processingStatusService = $processingStatusService;
        $this->processStatus = [];
        $this->allActiveEmailDistributionSystems = [];
    }


    /**
     * <b>triggered by CM CRON JOB campaign-treaction/cron/FtpUploadByPeppmtFtpUploadPlaner.php</b><br>
     * removes unwanted records from a csv file and stores it as redis hash entry <code>HSET  $transaction_id  $csvFilename "clean;csv;content"</code>
     * <br><b>data structure of csv files:</b><br><code>'EMAIL;ID;HERKUNFTID;ANREDE;VORNAME;NACHNAME;STRASSE;PLZ;ORT;LAND;GEBURTSDATUM;PARTNER;DOI_DATE;DS_ID;PUB_ID'</code>
     * @param string $contentCsv
     * @param string $csvFilename
     * @param int    $mandant_id
     * @param string    $transaction_id
     * @param bool   $updateExistingRecords
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @internal CsvProcessingService::processCsv is the first step for Campaign-Managers "Verteilerlisten"-csv-file <b>PRE-PROCESSING</b>
     * @see      https://treaction.atlassian.net/browse/WEB-5673
     */
    public function preProcessCsv(string $contentCsv, string $csvFilename, int $mandant_id, string $transaction_id, bool $updateExistingRecords): array
    {
        try {
            // get peppmt.mandant record by peppmt.mandant.id ($mandant_id)
            $mandantRecord = $this->getMandantRecord($mandant_id);
            $this->logger->info('$mandantRecord', [$mandantRecord['distribution_list_mandant_ids'], __METHOD__, __LINE__]);

            // init $allActiveMaileonEmailDistributionSystems
            $this->allActiveEmailDistributionSystems = $this->getAllActiveEmailDistributionSystems($mandantRecord['distribution_list_mandant_ids'],'Maileon');
            $this->logger->info('$this->allActiveEmailDistributionSystems', [$this->allActiveEmailDistributionSystems, __METHOD__, __LINE__]);

            // generate status array[mandant_id][versandsystem_id] = App\Services\ProcessingStatusService to collect all kind of properties of a received csv file
            // each [peppmt.mandant.id ][versandsystem.id] represents one object of App\Services\ProcessingStatusService (which is actually an entity without DB relation)
            $this->processStatus = $this->getMandantVersandsystemProcessStatusArray($this->allActiveEmailDistributionSystems);
            $this->addCustomFieldsToEsp($this->processStatus); // add all required customfields (e.g. delivery-id to the esp system e.g. Maileon)


            // remove csv internal duplicates
            $recordContent = $this->removeCsvBadRecords($contentCsv, $this->processStatus);
            $this->logger->info('$recordContent', [$recordContent, __METHOD__, __LINE__]);

            // convert unique record set to array for further processing
            if (!empty($recordContent)) {
                // get DeliveryId from (simple) sequence => see src/Sequences
                $deliveryId = $this->getSequenceNextValue($mandantRecord['distribution_list_delivery_id_sequence_name']);
                $keyValuePairRecords = $this->getEnrichedKeyValuePairRecords($recordContent, $deliveryId);

                $this->logger->info('$keyValuePairRecords', [$keyValuePairRecords, __METHOD__, __LINE__]);
            } else {
                $this->logger->warning('removeCsvBadRecords removed all records, nothing to process', [__METHOD__, __LINE__]);
                throw new \Exception('removeCsvBadRecords removed all records, nothing to process' . ', method=' . __METHOD__ . ', line=' . __LINE__);
            }

            // count blacklistHits into $this->processStatus for each mandant:versandsystem-instance, remove blacklisted records from &$keyValuePairRecords
            $timeForBlacklistCheck = $this->removeRecordsOfBlacklistedEmails($keyValuePairRecords, $mandantRecord['type']); // $keyValuePairRecords array might get shorter here
            $this->logger->info('$keyValuePairRecords', ['count' => count($keyValuePairRecords), 'process time [s]' => $timeForBlacklistCheck, __METHOD__, __LINE__]);

            // with the remaining rest of records do the following:
            // - count the total of unsubscribers of each single active mandant-versandsystem-combination from abmelder_ems.mandant_<id> joined with asp_<id>, but do not delete them in $keyValuePairRecords,
            //   - enter the total of unsubscribers into \App\Services\ProcessingStatusService::setCountAbmelderHits
            //   - update field "AbmelderHits" in $keyValuePairRecords
            //   - activate all unsubscribers in corresponding email delivery systems (ESP) by maileon api
            // - count the total of already existing email addresses (<mandant-database>.history) of each single mandant
            //   - enter count of total existing leads (email addresses) in history into \App\Services\ProcessingStatusService::setCountExistingLeadsInHistory
            $timeForUnsubscribersExistingEmailsCheck = $this->detectUnsubscribersAndExistingEmails($keyValuePairRecords, $mandantRecord['id'], $this->cmDbUser, $this->cmDbPassword, $csvFilename);
            $this->exchangeFieldValuesAndDoFurtherEnrichments($keyValuePairRecords, $mandant_id, $this->cmDbUser, $this->cmDbPassword, $csvFilename); // returns void
            // change $keyValuePairRecords back to csv, (may be) store the zipped csv in some directory or as longtext in some database-table, return the csv
            $distributionCsvArray = $this->getKeyValuePairRecordsAsCsv($keyValuePairRecords);


            $this->logger->info('$this->processStatus', [$this->processStatus, __METHOD__, __LINE__]);
            $dbStats = $this->setFileStatistics($mandantRecord['id'], $csvFilename, $transaction_id, count($keyValuePairRecords), []);
            $this->logger->info('before if $distributionCsvArray', [$distributionCsvArray, __METHOD__, __LINE__]);

            if(null !== $distributionCsvArray) {
                // on CM-DB: remove all email records from <mandantdb>.history that won't be transmitted to ESP systems, since old CM code adds all except those with incomplete or to many record fields
                $this->logger->info('before removeRecordsFromHistoryTable', [__METHOD__, __LINE__]);
                $this->peppmtRepository->removeRecordsFromHistoryTable($this->cmDbUser, $this->cmDbPassword, $mandantRecord['id'], $this->getAllEmailAddresses($keyValuePairRecords), $csvFilename);

                // add the csvFile header to redis
                $this->setToRedis($transaction_id.':header', $csvFilename, $distributionCsvArray['header']);
                // add the csvFile content to redis
                $this->setToRedis($transaction_id.':content', $csvFilename, $distributionCsvArray['content']);
                // remember list of ESP to distribute to, simply overwrite for each transaction_id, since the mandant and it's ESP are the same for one transaction_id
                $this->setToRedis($transaction_id.':allActiveEmailDistributionSystems', 'json', json_encode($this->allActiveEmailDistributionSystems));

            } else { // no records left
                return [
                    'status' => false,
                    'timeForBlacklistCheck'=> $timeForBlacklistCheck,
                    'timeForUnsubscribersExistingEmailsCheck' => $timeForUnsubscribersExistingEmailsCheck ,
                    // 'statusDistribution' => [],
                    // 'processStatus' => $this->getProcessStatusAsJsonOrArray('array'),
                    'statistics' => $dbStats,
                ];
            }

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            throw new \Exception('exception=' . $e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }

        $this->logger->info('$this->getProcessStatusAsString', [$this->getProcessStatusAsJsonOrArray(), __METHOD__, __LINE__]);
        $this->logger->info('$dbStats', [$dbStats, __METHOD__, __LINE__]);

        return [
            'status' => true,
            // 'csv' => $distributionCsv,
            'records' => $keyValuePairRecords,
            'incompleteRecords' => $this->getIncompleteRecords(),
            'timeForBlacklistCheck'=> $timeForBlacklistCheck,
            'timeForUnsubscribersExistingEmailsCheck' => $timeForUnsubscribersExistingEmailsCheck ,
            // 'statusDistribution' => $statusDistribution,
            // 'processStatus' => $this->getProcessStatusAsJsonOrArray('array'),
            // 'statistics' => $dbStats,
        ];
    }


    /**
     * triggered by CM CRON JOB campaign-treaction/cron/FtpUploadByPeppmtFtpUploadPlaner.php
     * @param int    $mandantId
     * @param string $transactionId
     * @return array
     * @throws \Exception
     */
    public function consolidateAndFtpUpload(int $mandantId, string $transactionId):array
    {
        // TODO: add code: get files from redis, combine them into one file and send them to corresponding esp
        $bruttoRecords = 0;
        $nettoRecords = 0;
        $allSuccessfull=true;


        try {
            $csvContentOfTransaction = $this->consolidate($transactionId, $bruttoRecords, $nettoRecords);
        } catch (Exception $e) {
            throw new \Exception($e->getMessage()); // propagate exception
        }

        $this->logger->info('consolidate', [$csvContentOfTransaction, $bruttoRecords, $nettoRecords]);
        $uploadFileCsvFileName = 'TID-'.substr($transactionId, 0, 8).'-'.date('YmdHi').'-uploadservice.csv';

        // run the distribution of the csv file (optimal in parallel by guzzle async post request to this service) to each mandant:versandsystem-combination
        if(null !== $csvContentOfTransaction) {
            $this->allActiveEmailDistributionSystems = $this->getRedisAllActiveEmailDistributionSystems($transactionId); // needed in next call of $this->distributeCsvToEsp !!!
            $statusDistribution = $this->distributeCsvToEsp($csvContentOfTransaction, $uploadFileCsvFileName, $allSuccessfull);
            // set status in ftp_upload_planer
            $this->peppmtRepository->setFtpUploadPlanerUploadStatus($mandantId, $transactionId, $uploadFileCsvFileName, ($statusDistribution)??[] );

            $allCsvFilenames = $this->redisGetAllFilenames($transactionId);
            $this->peppmtRepository->updateTableImportDateiStatusToUploaded($this->cmDbUser, $this->cmDbPassword, $mandantId, $allCsvFilenames);
            $this->deleteRedis($transactionId);
        }

        if( !$allSuccessfull) {
            return [
                'status' => false,
                'message' => 'all or some uploads failed. check statusDistribution.',
                'uploadFileCsvFileName' => $uploadFileCsvFileName,
                'bruttoRecords' => $bruttoRecords,
                'nettoRecords' => $nettoRecords,
                'statusDistribution' => $statusDistribution,
            ];
        }

        return [
            'status' => true,
            'message' => 'all uploads successful',
            'uploadFileCsvFileName' => $uploadFileCsvFileName,
            'bruttoRecords' => $bruttoRecords,
            'nettoRecords' => $nettoRecords,
            'statusDistribution' => $statusDistribution,
        ];

    }


    /**
     * get all filenames of a transactionId
     * @param string $transactionId
     * @return array
     */
    private function redisGetAllFilenames(string $transactionId):array
    {
        $allFilenames = [];
        $redis = new \Redis();
        $redis->connect($this->redisHost, $this->redisPort);
        $connStatus = $redis->auth([$this->redisUser, $this->redisPw]);
        $this->logger->info('$connStatus redis', [$connStatus, __METHOD__, __LINE__]);

        $filenames = $redis->hGetAll($transactionId.':header');

        foreach ($filenames as $filename => $value) {
            $this->logger->info('$filename', [$filename, __METHOD__, __LINE__]);
            $allFilenames[] = $filename;
        }
        return $allFilenames;
    }


    private function deleteRedis(string $transactionId)
    {
        $redis = new \Redis();
        $redis->connect($this->redisHost, $this->redisPort);
        $connStatus = $redis->auth([$this->redisUser, $this->redisPw]);
        $this->logger->info('$connStatus redis', [$connStatus, __METHOD__, __LINE__]);

        $redis->hDel($transactionId.':allActiveEmailDistributionSystems', 'json' );

        $allHeader = $redis->hGetAll($transactionId.':header');
        $allContent = $redis->hGetAll($transactionId.':content');
        $delHeader = 0;
        $delContent = 0;

        foreach ($allHeader as $key => $value) {
            $delHeader += $redis->hDel($transactionId.':header', $key );
            $this->logger->info('$allHeader delete ', ['KEY' => $key, $delHeader, __METHOD__, __LINE__]);
        }

        foreach ($allContent as $key => $value) {
            $delContent += $redis->hDel($transactionId.':content', $key );
            $this->logger->info('$allContent delete', ['KEY' => $key, $delContent, __METHOD__, __LINE__]);
        }

        return ['$allHeader' => $allHeader, '$allContent' => $allContent];
    }


    /**
     * consolidates all content records of all transaction files, checks if they all have the same header.<br><b>if headers are different, an exception is raised  and FTP-Upload is stopped</b><br>
     * sets values of $bruttoRecords and $nettoRecords, if provided -> check is done on top just in case if different files have same records - which shouldn't be the case due to history check in "processCsv" Step
     * @param string $transactionId
     * @param int    $bruttoRecords (optional)
     * @param int    $nettoRecords (optional)
     * @return string
     * @throws Exception
     */
    private function consolidate(string $transactionId, int &$bruttoRecords=0, int &$nettoRecords=0):string
    {
        $redis = new \Redis();
        $redis->connect($this->redisHost, $this->redisPort);
        $connStatus = $redis->auth([$this->redisUser, $this->redisPw]);
        $this->logger->info('$connStatus redis', [$connStatus, __METHOD__, __LINE__]);

        $sets = $redis->hGetAll($transactionId.':content');
        $headers = $redis->hGetAll($transactionId.':header');

        // echo "\nsets\n" . print_r($sets, true);
        $content = '';
        $uniqueHeaders = [];
        $uniqueRecords = [];

        ///// HEADER SECTION /////

        // collect all headers in an array
        foreach ($headers as $recordset) {
            // echo "\nrecordsset\n$recordset";
            $records = explode("\n", $recordset);
            foreach($records as $record) {
                $uniqueHeaders[] = $record;
            }
        }

        $this->logger->info('$uniqueHeaders collected', [$uniqueHeaders, __METHOD__, __LINE__]);


        // remove eventually empty headers records
        foreach ($uniqueHeaders as $key => $record) {
            if(empty($record)) {
                unset($uniqueHeaders[$key]);
            }
        }


        // make all records unique -> exactly one header record should be left
        $uniqueHeaders = array_unique($uniqueHeaders);
        sort($uniqueHeaders);
        $this->logger->info('$uniqueHeaders unique', [$uniqueHeaders, __METHOD__, __LINE__]);

        // exit if there are different headers
        if(count($uniqueHeaders) > 1) {
            $message = 'To be consolidated and uploaded files have different headers. No consolidation and therefore no upload possible. check and adapt the files that where imported.';
            $this->logger->error($message, [__METHOD__,__LINE__]);
            $this->logger->error('NON $uniqueHeaders', [$uniqueHeaders, __METHOD__,__LINE__]);
            throw new Exception($message.', method='.__METHOD__.', line='.__LINE__);
        }

        $header = $uniqueHeaders[0];
        $this->logger->info('$header', [$header, __METHOD__, __LINE__]);


        ///// CONTENT SECTION /////

        // collect all records in an array
        foreach ($sets as $recordset) {
            // echo "\nrecordsset\n$recordset";
            $records = explode("\n", $recordset);
            foreach($records as $record) {
                $uniqueRecords[] = $record;
            }
        }

        // remove empty records
        foreach ($uniqueRecords as $key => $record) {
            if(empty($record)) {
                unset($uniqueRecords[$key]);
            }
        }
        $bruttoRecords = count($uniqueRecords);
        $this->logger->info('count $bruttoRecords', [$bruttoRecords, __METHOD__, __LINE__]);



        // make all records unique
        $uniqueRecords = array_unique($uniqueRecords);
        sort($uniqueRecords);
        $nettoRecords = count($uniqueRecords);
        $this->logger->info('$nettoRecords', [$nettoRecords, __METHOD__, __LINE__]);

        // back from array to csv
        $contentRecordSet = implode("\n", $uniqueRecords);

        $content = rtrim($contentRecordSet, "\n")."\n"; // ensure there is only one "\n"

        return  $header.self::LINE_SEPARATOR_N.$content;
    }



    private function setToRedis(string $key_transaction_id, string $field, string $value)
    {
        $redis = new \Redis();
        $redis->connect($this->redisHost, $this->redisPort);
        $connStatus = $redis->auth([$this->redisUser, $this->redisPw]);

        $this->logger->info('$connStatus', [$connStatus, __METHOD__,__LINE__]);

        $status = $redis->hSet($key_transaction_id, $field, $value);

        //  - 1 if value didn't exist and was added successfully, - 0 if the value was already present and was replaced, FALSE if there was an error.

        switch ($status) {
            case (bool) false: $info = 'ERROR'; break;
            case (int) 1: $info = 'value didnt exist and was added successfully'; break;
            case (int) 0: $info = 'value was already present and was replaced'; break;
            default: $info = '???'; break;
        }

        $this->logger->info('redis hset status', [$status, $info, $key_transaction_id, $field, $value, __METHOD__,__LINE__]);

    }


    /**
     * redundant code from removeCsvBadRecords, but production of this app-change is required to be fast now ...
     * @param string $contentCsv csv file with header
     * @return array[] of records
     */
    private function getRecordStorage(string $contentCsv)
    {
        $lnr = 1;
        $lines = explode(self::LINE_SEPARATOR_RN, $contentCsv);
        $recordStorage = [];
        $arrayIncompleteRecords=[];

        foreach ($lines as $line) {
            if (1 === $lnr) { // get the key line of csv
                $keys = explode(';', $line);
                foreach ($keys as $index => $key) {
                    if (strtoupper($key) === 'EMAIL') {
                        $emailIndex = $index;
                        // $this->logger->info('$emailIndex', [$emailIndex, __METHOD__, __LINE__]);
                    }
                }
            } else {  // get the value lines of csv

                $values = explode(';', $line);
                $this->logger->info('$values countBrutto', [$values, __METHOD__, __LINE__]);
                if (!empty($values) && !empty($values[0])) { //!empty($values[0]) => empty last line like "\n\r"
                    $countBrutto++;
                }

                if (count($keys) === count($values)) {
                    $checkDuplicateEmails[] = $values[$emailIndex];
                    $recordStorage[] = $values;
                } else {
                    if (!empty($values) && count($values) > 1) {
                        $arrayIncompleteRecords[] = $values;
                    }
                }
            }// end get the value lines of csv
            $lnr++;
        } // end foreach ($lines as $line)

        return ['recordStorage' => $recordStorage, 'arrayIncompleteRecords' => $arrayIncompleteRecords];
    }


    /**
     * @param string $contentCsv
     * @param array  $processStatusArray
     * @return array array <b>"recordContent"</b> ['keys' => key-array, 'recordStorage' => records-array]
     * @internal remove records with duplicate email, leave the first email-record of duplicates, remove records with wrong field count, set determined counting values<sup>1)</sup> to $this->processStatus
     *             <br><sup>1)</sup>
                    <br>setCountInSetDuplicates($removedDuplicates);
                    <br>setCountBrutto($countBrutto);
                    <br>setCountNonWhiteListHits($countNonWhitelistDomains);
                    <br>setArrayIncompleteRecords($arrayIncompleteRecords);
                    <br>setArrayNonWhitelistRecords($arrayNonWhitelistRecords);
     */
    private function removeCsvBadRecords(string $contentCsv, array &$processStatusArray): array
    {
        $checkDuplicateEmails = [];
        $duplicateEmails = [];
        $keys = [];
        $recordStorage = [];
        $arrayIncompleteRecords = [];
        $arrayNonWhitelistRecords = [];

        $countBrutto = 0;
        $countNonWhitelistDomains = 0;
        $unsetNonWhitelistDomain = false;

        $emailIndex = -1;
        $lnr = 1;
        $removedDuplicates = 0;

        // get whitelist domains array, throw exception if something goes wrong
        try {
            $domainWhitelist = $this->peppmtRepository->getDomainWhitelist();
            if (null === $domainWhitelist) {
                throw new \Exception('Processing stopped. Empty recordset in peppmt.whitelist_domains. method=' . __METHOD__ . ', line=' . __LINE__);
            }
        } catch (\Exception $e) {
            $this->logger->error('Processing stopped. Could not get peppmt.whitelist_domains: ' . $e->getMessage(), [__METHOD__, __FILE__]);
            throw new \Exception('Processing stopped. Could not get peppmt.whitelist_domains: ' . $e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }


        // convert csv into array, store lines with same field count as keys into $recordStorage, others into $arrayIncompleteRecords, store all emails of correct length records into $checkDuplicateEmails
        $lines = explode(self::LINE_SEPARATOR_RN, $contentCsv);
        foreach ($lines as $line) {
            if (1 === $lnr) { // get the key line of csv
                $this->logger->info('header', [$line, __METHOD__, __LINE__]);
                $keys = explode(';', $line);
                foreach ($keys as $index => $key) {
                    if (strtoupper($key) === 'EMAIL') {
                        $emailIndex = $index;
                        $this->logger->info('$emailIndex', [$emailIndex, __METHOD__, __LINE__]);
                    }
                }
            } else {  // get the value lines of csv

                $values = explode(';', $line);
                unset($values[count($values)-1]); // THIS IS A BUGFIX: remove the last field, since it's not killed during creation of harmonized files in content records ==> get last ";" and remove it

                $this->logger->info('$values countBrutto', [$values, __METHOD__, __LINE__]);
                if (!empty($values) && !empty($values[0])) { //!empty($values[0]) => empty last line like "\n\r"
                    $countBrutto++;
                }

                if (count($keys) === count($values)) {
                    $checkDuplicateEmails[] = $values[$emailIndex];
                    $recordStorage[] = $values;
                } else {
                    if (!empty($values) && count($values) > 1) {
                        $arrayIncompleteRecords[] = $values;
                        // "notexists_2-02-ZZZZXXXX@web.de","31007272","1-24050","Herr","Not1","Exists1","Test1str. 11","17098","Friedland","DE","30.11.1951","18","04.12.2021 11:18","nil","nil","nil"
                    }
                }
            }// end get the value lines of csv
            $lnr++;
        } // end foreach ($lines as $line)

        $this->logger->info('$recordStorage', [$recordStorage, __METHOD__, __LINE__]);
        $this->logger->info('$recordStorage count', [count($recordStorage), __METHOD__, __LINE__]);
        $this->logger->info('$domainWhitelist', [$domainWhitelist, __METHOD__, __LINE__]);

        // check for nonWhitelistDomains
        foreach ($recordStorage as $index => $record) {
            $domain = explode('@', $record[$emailIndex])[1];
            $this->logger->info('$domain', [$domain, __METHOD__, __LINE__]);


            if (!in_array($domain, $domainWhitelist)) {
                $arrayNonWhitelistRecords = $record;
                unset($recordStorage[$index]);
                $countNonWhitelistDomains++;
            }
        }

        $this->logger->info('$checkDuplicateEmails', [$checkDuplicateEmails, __METHOD__, __LINE__]);
        $this->logger->info('$recordStorage', [$recordStorage, __METHOD__, __LINE__]);
        $this->logger->info('$recordStorage count', [count($recordStorage), __METHOD__, __LINE__]);
        $this->logger->info('$arrayIncompleteRecords count', [count($arrayIncompleteRecords), __METHOD__, __LINE__]);

        // get duplicate emails
        foreach (array_count_values($checkDuplicateEmails) as $val => $count) {
            if ($count > 1) {
                $duplicateEmails[] = $val;
            }
        }

        $this->logger->info('$duplicateEmails', [$duplicateEmails, __METHOD__, __LINE__]);
        $this->logger->info('$duplicateEmails count', [count($duplicateEmails), __METHOD__, __LINE__]);

        // remove duplicate emails from array
        foreach ($duplicateEmails as $duplicateEmail) {
            $firstRecordFound = false;

            foreach ($recordStorage as $recordIndex => $v) {
                if ($firstRecordFound === false && $v[$emailIndex] === $duplicateEmail) {
                    $firstRecordFound = true;
                } elseif ($v[$emailIndex] === $duplicateEmail) {
                    unset($recordStorage[$recordIndex]);
                    $removedDuplicates++;
                } else {
                    false; // just do nothing
                }
            }
        }

        // set status for found duplicates and incomplete record structure
        //      implementation helper: remove line comments to access $vId methods
         //$versandsystem = new \App\Services\ProcessingStatusService();
         //$versandsystem->setArrayIncompleteRecords($arrayNonWhitelistRecords);
        $this->logger->info('$countBrutto', [$countBrutto, __METHOD__, __LINE__]);
        foreach ($processStatusArray as &$mId) {
            foreach ($mId as &$vId) {
                $vId->setCountInSetDuplicates($removedDuplicates);
                $vId->setCountBrutto($countBrutto);
                $vId->setCountNonWhiteListHits($countNonWhitelistDomains);
                $vId->setArrayIncompleteRecords($arrayIncompleteRecords);
                $vId->setArrayNonWhitelistRecords($arrayNonWhitelistRecords);
                // do not set the count netto already here, since there will more checks => blacklisted emails, unsubscribed emails
            }
        }

        $this->logger->info('$processStatusArray', [$processStatusArray, __METHOD__, __LINE__]);

        // rebuilt and return csv
        $this->logger->info('$keys', [$keys, __METHOD__, __LINE__]);
        $this->logger->info('$recordStorage', [$recordStorage, __METHOD__, __LINE__]);
        $this->logger->info('$recordStorage records left', [count($recordStorage), __METHOD__, __LINE__]);

        $csv = implode(';', $keys) . "\n";
        foreach ($recordStorage as $record) {
            $csv .= implode(';', $record) . "\n";
        }

        if ($_ENV['APP_ENV'] === 'dev') {
            file_put_contents(__DIR__ . '/../../var/log/cleanedUpCsv.csv', $csv); // TODO: may be remove this later or sve it in a different directory
        }

        return ['keys' => $keys, 'recordStorage' => $recordStorage];
    }


    /**
     * @param string $distribution_list_mandant_ids
     * @return array
     * @throws \Exception
     * @internal return array of [mandant_id][versandsystem_id] = clone $this->processingStatusService
     */
    private function getMandantVersandsystemProcessStatusArray(array $allActiveEmailDistributionSystems): array
    {
        $mandantStatusArray = [];

        foreach ($allActiveEmailDistributionSystems as $esp) {
            // versandsystem_type, mandant_id, versandsystem_id, versandsystem_asp, versandsystem_asp_abkz, versandsystem_ftpuser, versandsystem_ftppw
            $mandantStatusArray[$esp['mandant_id']][$esp['versandsystem_id']] = clone $this->processingStatusService;
        }

        $this->logger->info('$mandantStatusArray count', [count($mandantStatusArray), __METHOD__, __LINE__]);
        return $mandantStatusArray;
    }


    /**
     * returns array [versandsystem_type, mandant_id , versandsystem_id, versandsystem_asp , versandsystem_asp_abkz, versandsystem_ftpuser, versandsystem_ftppw]
     * @param string $versandsystem_type default = 'Maileon'
     * @throws \Exception
     */
    private function getAllActiveEmailDistributionSystems(string $distribution_list_mandant_ids, string $versandsystem_type='Maileon'):array
    {
        try {
            return $this->peppmtRepository->getAllActiveEmailDistributionSystems($distribution_list_mandant_ids, $versandsystem_type);
        } catch (Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }
    }


    /**
     * @param string $transactionId
     * @return array
     */
    private function getRedisAllActiveEmailDistributionSystems(string $transactionId):array
    {
        $redis = new \Redis();
        $redis->connect($this->redisHost, $this->redisPort);
        $connStatus = $redis->auth([$this->redisUser, $this->redisPw]);
        $this->logger->info('$connStatus redis', [$connStatus, __METHOD__, __LINE__]);

        $sets = $redis->hGetAll($transactionId.':content');

        $allActiveEmailDistributionSystemsJson = $redis->hGet($transactionId.':allActiveEmailDistributionSystems', 'json');
        $this->logger->info('$allActiveEmailDistributionSystemsJson',[$allActiveEmailDistributionSystemsJson, __METHOD__, __LINE__]);
        $allActiveEmailDistributionSystems = json_decode($allActiveEmailDistributionSystemsJson, true, 512, JSON_OBJECT_AS_ARRAY);
        $this->logger->info('$allActiveEmailDistributionSystems',[$allActiveEmailDistributionSystems, __METHOD__, __LINE__]);
        return $allActiveEmailDistributionSystems;
    }


    /**
     * @param int    $mandant_id
     * @param string $mandant_name
     * @param string $mandant_abkz
     * @return array|null [id,mandant,package,abkz,unsubkey,dm_rep_mail,dm_rep_betreff,untermandant,type,db_user,db_pw,db_name,distribution_list_mandant_ids,distribution_list_delivery_id_sequence_name,active]
     * @throws \Exception
     * @internal get a peppmt.mandant record by either mandant_id or mandant_name or mandant_abkz, wraps PeppmtRepository::getMandantRecord and runs checks on expected fields<br>db_name<br>distribution_list_mandant_ids<br>distribution_list_delivery_id_sequence_name<br>throws exception if fields are empty or record was not found
     */
    private function getMandantRecord(int $mandant_id = 0, string $mandant_name = "", string $mandant_abkz = ""): array
    {
        try {
            $mandantRecord = $this->peppmtRepository->getMandantRecord($mandant_id, $mandant_name, $mandant_abkz);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            throw new \Exception($e->getMessage() . 'in method ' . __METHOD__ . ', line ' . __LINE__);
        }

        // do checks
        if (empty($mandantRecord)) {
            $this->logger->error("empty mandant record for $mandant_id", [__METHOD__, __LINE__]);
            throw new \Exception("empty mandant record for $mandant_id" . 'in method ' . __METHOD__ . ', line ' . __LINE__);
        }
        if (empty($mandantRecord['db_name'])) {
            $this->logger->error("db_name not set for mandant $mandant_id", [__METHOD__, __LINE__]);
            throw new \Exception("db_name not set for mandant $mandant_id" . 'in method ' . __METHOD__ . ', line ' . __LINE__);
        }
        if (empty($mandantRecord['distribution_list_mandant_ids'])) {
            $this->logger->error("distribution_list_mandant_ids not set for mandant $mandant_id", [__METHOD__, __LINE__]);
            throw new \Exception("distribution_list_mandant_ids not set for mandant $mandant_id" . 'in method ' . __METHOD__ . ', line ' . __LINE__);
        }
        if (empty($mandantRecord['distribution_list_delivery_id_sequence_name'])) {
            $this->logger->error("distribution_list_delivery_id_sequence_name not set for mandant $mandant_id", [__METHOD__, __LINE__]);
            throw new \Exception("distribution_list_delivery_id_sequence_name not set for mandant $mandant_id" . 'in method ' . __METHOD__ . ', line ' . __LINE__);
        }

        return $mandantRecord;
    }


    /**
     * convert $recordContent-array from <b>CsvProcessingService::removeCsvBadRecords</b> into an array of records with key:value pairs, <b>additional fields deliveryId, AbmelderHits, BlacklistHits</b> and therefore no header for further processing
     * @param array $recordContent ['keys' => key-array, 'recordStorage' => array of records as array]
     * @param int   $deliveryId
     * @return array
     * @throws \Exception
     * @internal expects the same amount of keys as values in each record array
     */
    private function getEnrichedKeyValuePairRecords(array $recordContent, int $deliveryId, bool $enriched=true): array
    {
        $sameNumberOfFields = true;
        $recordIndex = [];
        $dataArr = [];

        // check for same field count of keys and record fields
        $countKeys = count($recordContent['keys']);

        foreach ($recordContent['recordStorage'] as $index => $recordFields) {
            $this->logger->info('compare', [$countKeys, count($recordFields), $index, __METHOD__, __LINE__]);

            if ($countKeys !== count($recordFields)) {
                $sameNumberOfFields = false;
                $recordIndex[] = $index;
            }
        }


        if ($sameNumberOfFields) {
            foreach ($recordContent['recordStorage'] as $record) {
                $dataArr[] = array_combine($recordContent['keys'], $record);
            }
        } else {
            $message = 'In this processing state number of keys and fields of each record should be the same, but they are not.';

            $this->logger->error($message, [__METHOD__, __LINE__]);

            foreach ($recordIndex as $index) {
                $this->logger->error('Error in record', [$recordContent['recordStorage'][$index], __METHOD__, __FILE__]);
            }
            // exit here
            throw new \Exception("$message method=" . __METHOD__ . ', line=' . __LINE__);
        }

        $this->logger->info('$dataArr', [$dataArr, __METHOD__, __LINE__]);


        // enrich records by fields deliveryId, AbmelderHits and BlacklistHits
        if($enriched) {
            foreach ($dataArr as &$record) {
                $record['DeliveryId'] = $deliveryId;
                $record['HistoryID'] = 0;
                //$record['BlacklistHits'] = 0;
                //$record['ExistingLeads'] = 0;
                //$record['NonWhitelistHits'] = 0;
            }
        }

        $this->logger->info('return', [$dataArr, __METHOD__, __LINE__]);
        return $dataArr;
    }


    /**
     * 1. counts into \App\Services\ProcessingStatusService::countBlacklistHits<br>2. unsets (removes) blacklisted records from call by reference array &$keyValuePairRecords<br>3. updates remaining records of $keyValuePairRecords-field "BlacklistHits" with the total amount of found blacklist hits
     * @param array  $keyValuePairRecords
     * @param string $mandant_type
     * @return float time in [s] taken for the blacklist check
     * @throws \Exception
     */
    private function removeRecordsOfBlacklistedEmails(array &$keyValuePairRecords, string $mandant_type): float
    {
        $t1 = microtime(true);
        $allEmailAddresses = $this->getAllEmailAddresses($keyValuePairRecords);
        $blacklistTable = ('intern' === trim(strtolower($mandant_type))) ? 'Blacklist' : 'Blacklist_2';
        $countBlacklistHits = 0;

        try {
            foreach ($allEmailAddresses as $index => $emailAddress) {
                if ($this->peppmtRepository->isBlacklisted($emailAddress, $blacklistTable)) {
                    unset($keyValuePairRecords[$index]); // $keyValuePairRecords has same index as $allEmailAddresses, since index of $allEmailAddresses is derived from $keyValuePairRecords
                    $countBlacklistHits++;
                }
            }

            // $x = new \App\Services\ProcessingStatusService(); $x->setCountBlacklistHits($countBlacklistHits);
            // set value for each mandant_id, versandsystem_id instance
            foreach ($this->processStatus as $versandsystem) {
                foreach ($versandsystem as $processingStatusService) {
                    $processingStatusService->setCountBlacklistHits($countBlacklistHits);
                }
            }

            // don't run: set BlacklistHits to each record of $keyValuePairRecords
            // foreach ($keyValuePairRecords as &$record) {
            //     $record['BlacklistHits'] = $countBlacklistHits;
            // }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            throw new \Exception('exception=' . $e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }

        $t2 = microtime(true);
        return ($t2 - $t1);
    }


    /**
     * <b>Unsubscribers</b><br>
     * - count the total number of unsubscriber emails from abmelder_ems.mandant_<id> joined with asp_<id>, but do not delete them in $keyValuePairRecords,
     * - enter the total number of unsubscribers into \App\Services\ProcessingStatusService::setCountAbmelderHits
    * - update field "AbmelderHits" in $keyValuePairRecords
    * - activate all unsubscribers in corresponding email delivery systems (ESP) by maileon api
     *
     * <br><b>Existing email addresses</b><br>
    * - count the total of already existing email addresses (<mandant-database>.history)
    * - enter count of total existing leads (email addresses) in history into \App\Services\ProcessingStatusService::setCountExistingLeadsInHistory
     *
     * @param array $keyValuePairRecords
     * @return float
     * @throws \Exception
     */
    private function detectUnsubscribersAndExistingEmails(array &$keyValuePairRecords, int $mandantId, string $cmDbUser, string $cmDbPw, string $csvFilename): float
    {
        $t1 = microtime(true);
        $unsubscriberEmails = [];

        try {
            $allEmailAddresses = $this->getAllEmailAddresses($keyValuePairRecords);
            $countExistingLeadsInHistoryPerEmail = [];
            $countUnsubscriberHitsPerEmail = [];
            // $this->logger->info('LINE# ', [__LINE__]);
            // get previously imported emailAddresses ("ExistingLeadsInHistory") and unsubscribers from CM-DB by emailAddresses


            foreach ($allEmailAddresses as $indexEmailAddress => $emailAddress) {
                // $this->logger->info('LINE# ', [__LINE__]);
                // $this->logger->info("emailAddress : $emailAddress", [__METHOD__, __LINE__]);

                $countExistingLeadsInHistoryPerEmail[$emailAddress] = 0;
                $countUnsubscriberHitsPerEmail[$emailAddress] = 0;

                foreach ($this->processStatus as $mandantId => $versandsystem) {

                    $countExistingLeadsInHistoryPerEmail[$emailAddress] += ($this->peppmtRepository->isExistingExistingLeadsInHistory($this->cmDbUser, $this->cmDbPassword, $mandantId, $emailAddress, $csvFilename))
                        ? 1
                        : 0;
                    $countUnsubscriberHitsPerEmail[$emailAddress] += ($this->peppmtRepository->isUnsubscribed($this->cmDbUser, $this->cmDbPassword, $mandantId, $emailAddress))
                        ? 1
                        : 0;


                    //  COMMENT: mike@2022-02-22: do not reactivate unsubscribers in target ESP systems, since this is done by the import
                    /*
                    // reactivate unsubscribed email in corresponding (currently only) maileon account
                    if (1 === $countUnsubscriberHitsPerEmail[$emailAddress]) {
                        foreach ($versandsystem as $versandsystemId => $processStatus) {

                            $unsubscriberEmails[$emailAddress][] = [
                                'mandantId' => $mandantId,
                                'versandsystemId' => $versandsystemId,
                                'firstname' => array_values($keyValuePairRecords)[$indexEmailAddress]['VORNAME'] ?? 'NoFirstnameInRecord',
                                'lastname' => array_values($keyValuePairRecords)[$indexEmailAddress]['NACHNAME'] ?? 'NoLastnameInRecord',
                            ];
                        }
                    }
                    */
                }
            } // END foreach ($allEmailAddresses as $indexEmailAddress => $emailAddress) {

            $this->logger->info('$countExistingLeadsInHistoryPerEmail', [$countExistingLeadsInHistoryPerEmail, __METHOD__, __LINE__]);
            $this->logger->info('$countUnsubscriberHitsPerEmail', [$countUnsubscriberHitsPerEmail, __METHOD__, __LINE__]);
            $this->logger->info('$unsubscriberEmails', [$unsubscriberEmails, __METHOD__, __LINE__]);
            $this->logger->info('$countUnsubscriberHitsPerEmail', [$countUnsubscriberHitsPerEmail, __METHOD__, __LINE__]);
            // $this->logger->info('LINE# ', [__LINE__]);

            //  COMMENT: mike@2022-02-22: do not reactivate unsubscribers in target ESP systems
            /*
            $reActivated = [];
            foreach($unsubscriberEmails as $emailAddress => $esp) {

                foreach ($esp as $connect) {
                    // $this->logger->info('LINE#  $connect=', [$connect, __LINE__]);
                    $reActivated[$emailAddress] = $this->reactivateUnsubscriber(
                        $connect['mandantId'],
                        $connect['versandsystemId'],
                        $emailAddress,
                        $connect['firstname'],
                        $connect['lastname']
                    );
                }
            }
            */

            // $this->logger->info('$reActivated', [$reActivated,__METHOD__, __LINE__]);
            // COMMENT REPLACE START $totalCountUnsubscriberHits / $totalCountExistingLeadsInHistory
            /*
             // count existing leads of history -> each email address counts only once!
             $totalCountExistingLeadsInHistory = 0;

            // in $countExistingLeadsInHistoryPerEmail there are no duplicated email address as $allEmailAddresses may have, but an $countExistingLeadsInHistoryPerEmail-emailAddress perhaps has a count > 1
            foreach ($countExistingLeadsInHistoryPerEmail as $emailAddress => $count) {
                $totalCountExistingLeadsInHistory += ($count > 0) ? 1 : 0;
            }

            // $this->logger->info('LINE# ', [__LINE__]);
            // count the unsubscribed emails -> each email address counts only once!
            $totalCountUnsubscriberHits = 0;
            foreach ($countUnsubscriberHitsPerEmail as $emailAddress => $count) {
                if($count>0) {
                    $totalCountUnsubscriberHits++;
                }
            }
            */
            // COMMENT REPLACE END $totalCountUnsubscriberHits / $totalCountExistingLeadsInHistory

            $totalCountUnsubscriberHits = 0;
            $totalCountExistingLeadsInHistory = 0;

            foreach ($allEmailAddresses as $indexEmailAddress => $emailAddress) {
                if(  $countExistingLeadsInHistoryPerEmail[$emailAddress] > 0 && $countUnsubscriberHitsPerEmail[$emailAddress] > 0 ) { // dublette und vorher übermittelt und unsubscribed => kontakt übermitteln
                    $totalCountExistingLeadsInHistory++;
                    $totalCountUnsubscriberHits++;
                } elseif ($countExistingLeadsInHistoryPerEmail[$emailAddress] === 0 && $countUnsubscriberHitsPerEmail[$emailAddress] > 0) { // keine dublette und vorher übermittelt und unsubscribed => (kommt eigentlich nicht vor)
                    $totalCountUnsubscriberHits++;
                } elseif ($countExistingLeadsInHistoryPerEmail[$emailAddress] > 0 && $countUnsubscriberHitsPerEmail[$emailAddress] === 0) { // dublette, aber nicht vorher übermittelt und nicht unsubscribed => kontakt nicht übermitteln
                    $totalCountExistingLeadsInHistory++;
                    foreach ($keyValuePairRecords as $index => $record) {
                        if($record['EMAIL'] === $emailAddress) {
                            $this->logger->info("REMOVING {$emailAddress} FROM TO BE SUBMITTED RECORDSET (dublette aber nicht in unsubscriber)", [__METHOD__, __LINE__]);
                            unset ($keyValuePairRecords[$index]); // TODO: check this TODAY (24.02.2022) !!!!
                        }
                    }
                } else { // !!! nothing to count !!!
                    $totalCountExistingLeadsInHistory += 0; // add 0
                    $totalCountUnsubscriberHits += 0; // add 0
                }
            }

            $this->logger->info('$totalCountExistingLeadsInHistory', [$totalCountExistingLeadsInHistory, __METHOD__, __LINE__]);
            $this->logger->info('$totalCountUnsubscriberHits', [$totalCountUnsubscriberHits, __METHOD__, __LINE__]);


            // $this->logger->info('LINE# $totalCountUnsubscriberHits', [$totalCountUnsubscriberHits, __LINE__]);
            // set processStatus for each mandant -- 1:n --> versandsystem
            foreach ($this->processStatus as $mandantId => &$versandsystem) { // loop over mandant
                // dev helper (comment out line below to get $x-methods for $processingStatusService) , because objects as array values can not be resolved by IDE:
                // $x= new \App\Services\ProcessingStatusService(); $x->setCountAbmelderHits($countUnsubscriberHitsPerEmail); $x->setCountExistingLeadsInHistory($countExistingLeadsInHistory); $x->setCountNetto($countNetto);
                foreach ($versandsystem as $versandsystemId => &$processingStatusService) {
                    $processingStatusService->setCountExistingLeadsInHistory($totalCountExistingLeadsInHistory);
                    $processingStatusService->setCountAbmelderHits($totalCountUnsubscriberHits);
                    $processingStatusService->setCountNetto(count($keyValuePairRecords));
                }
            }

            $this->logger->info('$this->processStatus', [$this->processStatus, __METHOD__, __LINE__]);
            // $this->logger->info('LINE# ', [__LINE__]);

        } catch (\Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception('message=' . $e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }

        $t2 = microtime(true);
        $this->logger->info('duration [s]', [($t2 - $t1), __METHOD__,__LINE__]);
        return ($t2 - $t1);
    }


    /**
     * @param array  $keyValuePairRecords
     * @param int    $mandantId
     * @param string $cmDbUser
     * @param string $cmDbPw
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function exchangeFieldValuesAndDoFurtherEnrichments(array &$keyValuePairRecords, int $mandantId, string $cmDbUser, string $cmDbPw, string $csvFilename): void
    {
        // field mapping and  enrichment of some fields
        // 'EMAIL;ID;HERKUNFTID;ANREDE;VORNAME;NACHNAME;STRASSE;PLZ;ORT;LAND;GEBURTSDATUM;PARTNER;DOI_DATE;DS_ID;PUB_ID'
        try {
            foreach ($keyValuePairRecords as &$record) {
                $historyIdDsIdHerkunft = $this->peppmtRepository->getHistoryIdAndDsIdAndHerkunft($this->cmDbUser, $this->cmDbPassword, $mandantId, $record['EMAIL'], $csvFilename);

                $this->logger->info('getHistoryIdAndDsIdAndHerkunft', ['$mandantId' => $mandantId, 'email' => $record['EMAIL'], 'OUTPUT: historyIdDsIdHerkunft' => $historyIdDsIdHerkunft, __METHOD__, __LINE__]);

                if(!empty($historyIdDsIdHerkunft)) {
                    $record['ID'] = $historyIdDsIdHerkunft['ID']; // <mandantDb>.history.ID
                    $record['HistoryID'] = $historyIdDsIdHerkunft['ID'];
                    $record['HERKUNFTID'] = $historyIdDsIdHerkunft['HERKUNFT'];
                    $record['DS_ID'] = $historyIdDsIdHerkunft['DS_ID']; // From <mandantDb>.history.DS_ID, value is initially contained in $record['ID']
                } else {
                    $this->logger->warning('$historyIdDsIdHerkunft: could not fetch record in history for ',['$mandantId' => $mandantId, 'email' => $record['EMAIL'], __METHOD__,__LINE__]);
                }
                $record['PARTNER'] = $this->peppmtRepository->getPartnerId($this->cmDbUser, $this->cmDbPassword, $mandantId, $record['EMAIL'], $csvFilename); // (varchar) get import_datei.PARTNER by csvFilename
                $this->logger->info('getPartnerId', ['$mandantId' => $mandantId, 'email' => $record['EMAIL'], 'OUTPUT: PARTNER' => $record['PARTNER'], __METHOD__, __LINE__]);
            }
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error('DBAl EXCEPTION '.$e->getMessage(), [__METHOD__, __LINE__]);
            throw new \Exception('ERROR IN METHOD '.__METHOD__ .', line '.__LINE__.': '.$e->getMessage() );
        }
    }


    /**
     * @param array $processStatus
     * @return void
     */
    private function addCustomFieldsToEsp(array &$processStatus):void
    {
        $addedCustomFields = [];
        foreach ($processStatus as $mandantId => $versandsystem) {
            foreach ($versandsystem as $versandsystemId => $processingStatusService) {
                $addedCustomFields[$mandantId][$versandsystemId] = $this->addCustomFields($mandantId, $versandsystemId);
            }
        }
        $this->logger->info('$addedCustomFields', [$addedCustomFields, __METHOD__, __LINE__]);
    }


    /**
     * @param int $mandant_id
     * @param int $versandsystem_id
     * @return false|array
     */
    private function addCustomFields(int $mandant_id, int $versandsystem_id):bool|array
    {
        $result = [];
        try {
            // get $espConnectionParams =[mandant_id, versandsystem_id, versandsystem_type, m_login, api_user, pw, secret_key]
            $espConnectionParams = $this->getEspConnectionParams($mandant_id, $versandsystem_id);

            // early exit
            if(empty($espConnectionParams)) {
                $this->logger->warning(" not get connection params for mandant=$mandant_id, versandsystem=$versandsystem_id", [__METHOD__, __LINE__]);
                return false;
            }

            // TODO (later): enhance with Promio, Empaction, Sendeffect, and so on
            if('Maileon' === $espConnectionParams['versandsystem_type']) {
                $config = ['apikey' => $espConnectionParams['m_login']];
            } else {
                $config = []; // currently, only Maileon is supported (2022-01)
            }

            $espService = $this->getEspService($espConnectionParams['versandsystem_type']);

            if(null !== $espService) {
                $result['addCustomFields'] = $espService->addCustomFields( $config); // corresponding custom fields are added automatically by Service Method
                $this->logger->info('$result addCustomFields', [$result, __METHOD__, __LINE__]);
            } else {
                $result['message'] = "espService for {$espConnectionParams['versandsystem_type']} could not be instantiated";
                $this->logger->warning('$result', [$result, __METHOD__, __LINE__]);
            }

            if (null === $result['addCustomFields']) {
                $result['message'] = " => reactivateUnsubscriber for email address {$emailAddress} in {$espConnectionParams['versandsystem_type']}/mandant_id={$mandant_id}/versandsystem_id={$versandsystem_id} failed: Message={$result['message']}";
                $this->logger->warning($result['message'], [__METHOD__, __LINE__]);
            }

        } catch(\Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            return false; // just return and go on
        }

        return $result;
    }


    /**
     * see configuration for EspServiceFactory, App\EspServices\MaileonService, App\EspServices\PromioService (not active) at config/services.yaml
     * @param int $mandant_id
     * @param int $versandsystem_id
     * @param string $emailAddress
     * @param string $firstname
     * @param string $lastname
     * @param string $versandsystem_type
     * @return bool|array
     * @deprecated since 2022-02-22
     */
    private function reactivateUnsubscriber(int $mandant_id, int $versandsystem_id, string $emailAddress, string $firstname, string $lastname, string $versandsystem_type='Maileon'): bool|array
    {
        $result = [];
        $result['status'] = false;

        try {
            // get $espConnectionParams =[mandant_id, versandsystem_id, versandsystem_type, m_login, api_user, pw, secret_key]
            $espConnectionParams = $this->getEspConnectionParams($mandant_id, $versandsystem_id);

            // early exit
            if(empty($espConnectionParams)) {
                $this->logger->warning(" not get connection params for mandant=$mandant_id, versandsystem=$versandsystem_id", [__METHOD__, __LINE__]);
                return false;
            }

            // TODO (later): enhance with Promio, Empaction, Sendeffect, and so on
            if('Maileon' === $espConnectionParams['versandsystem_type']) {
                $espServiceConnectionParams = ['apikey' => $espConnectionParams['m_login'], 'firstname' => $firstname, 'lastname' => $lastname];
            } else {
                $espServiceConnectionParams = []; // currently, only Maileon is supported (2022-01)
            }

            $espService = $this->getEspService($espConnectionParams['versandsystem_type']);

            if(null !== $espService) {
                $result = $espService->reactivateUnsubscriber($emailAddress, $espServiceConnectionParams);
                $this->logger->info('reactivateUnsubscriber', [$result, __METHOD__, __LINE__]);
            } else {
                $result['message'] = "espService for {$espConnectionParams['versandsystem_type']} could not be instantiated";
                $this->logger->warning($result['message'], [$result, __METHOD__, __LINE__]);
            }

            if (false === $result['status']) {
                $result['message'].= " => reactivateUnsubscriber for email address {$emailAddress} in {$espConnectionParams['versandsystem_type']}/mandant_id={$mandant_id}/versandsystem_id={$versandsystem_id} failed: Message={$result['message']}";
                $this->logger->warning($result['message'], [__METHOD__, __LINE__]);
            }

        } catch (\Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            return false; // just return and go on
        }

        $this->logger->info('$result espService', [$result, __METHOD__, __LINE__]);
        return $result['status'];
    }

    /**
     * @param string $versandsystem_type
     * @return EspServiceInterface
     * @throws \Exception
     */
    private function getEspService(string  $versandsystem_type):EspServiceInterface
    {
        return EspServiceFactory::createEspService(new VersandsystemType($versandsystem_type));
    }

    /**
     * @param int $mandant_id
     * @param int $versandsystem_id
     * @return array|null
     * @throws \Exception
     */
    private function getEspConnectionParams(int $mandant_id, int $versandsystem_id) : ?array
    {
        $espConnectionParams = $this->peppmtRepository->getEspConnection($mandant_id, $versandsystem_id);

        if( empty($espConnectionParams)) {
            $message = 'cannot get espConnectionParams';
            $this->logger->error($message, [$e->getMessage(), __METHOD__, __LINE__]);
            return null; // just return and go on
        }
        return $espConnectionParams;
    }
    /**
     * get sequence next value from sequence file name as defined in <b>peppmt.mandant.distribution_list_delivery_id_sequence_name</b>
     * from (simple) sequence content *.txt file (holds only an integer) below directory <b>src/Sequences</b>
     * value in <b>{$distribution_list_delivery_id_sequence_name}.txt</b> is incremented by 1
     * - <b>used by</b> App\Controller\CsvDistributionController::checkAlive<br>
     * - <b>used by</b> App\Services\CsvProcessingService
     * @param string $distribution_list_delivery_id_sequence_name
     * @return int
     */
    public function getSequenceNextValue(string $distribution_list_delivery_id_sequence_name): int
    {
        $fileName = $distribution_list_delivery_id_sequence_name . '.txt';
        $mandant_id = (int)str_replace('sq', '', $distribution_list_delivery_id_sequence_name);
        $this->logger->info(__DIR__ . "../Sequences/{$fileName}", [__METHOD__, __LINE__]);
        $val = (int)file_get_contents(__DIR__ . "/../Sequences/{$fileName}");
        $val++;
        file_put_contents(__DIR__ . "/../Sequences/{$fileName}", $val);
        return (int)($mandant_id . $val);
    }


    /**
     * <b>add kind og type you'd like to be returned (json, default or array)</b>
     * @param string $returnType <u>'json'</u>|'array'
     * @return string json|array
     * @internal helper method for logging
     */
    private function getProcessStatusAsJsonOrArray(string $returnType='json'): string|array
    {
        $tempArr = [];

        foreach ($this->processStatus as $mandantId => $versandsystem) {
            foreach ($versandsystem as $versandsystemId => $processingStatusService) {
                $tempArr[] = [
                    'mandantId' => $mandantId,
                    'versandsystemId' => $versandsystemId,
                    'processingStatusService' => $processingStatusService->getArray(),
                ];
            }
        }

        $this->logger->info('ENV='.$_ENV['APP_ENV'], [__METHOD__, __LINE__]);
        if ($_ENV['APP_ENV'] === 'dev') {
            file_put_contents(__DIR__ . '/../../var/log/processStatus.json', json_encode($tempArr, JSON_PRETTY_PRINT));
        }

        return ('json' === $returnType) ? json_encode($tempArr) :$tempArr;
    }


    /**
     * @return array
     * @internal get all incomplete records
     */
    private function getIncompleteRecords(): array
    {
        // implementation helper: remove line comments to access $vId methods
        //     $versandsystem = new \App\Services\ProcessingStatusService();
        //     $versandsystem->getArrayIncompleteRecords()

        foreach ($this->processStatus as $mandantId) {
            foreach ($mandantId as $versandsystemId) { // does and shall not loop!!!
                return $versandsystemId->getArrayIncompleteRecords(); // return 1st record found, since each combination of [mandantId][versandsystemId] contains same record set
            }
        }
        return []; // never reached but shows up as error in IDE, if not set
    }


    /**
     * returns <br><b>Either:</b><br>- numeric array[<i>record index</i>] = <i>(string) emailAddress</i> indexed by the index of $keyValuePairRecords, if $returnEmailAddressInKey is false (default).<br><b>Or:</b>
     * <br>- an array[<i>(string) emailAddress</i>] = <i>record index</i> of $keyValuePairRecords, if $returnEmailAddressInKey is true
     * @param array $keyValuePairRecords
     * @return array
     */
    private function getAllEmailAddresses(array $keyValuePairRecords, $returnEmailAddressInKey = false): array
    {
        $emailAddresses = [];
        if ($returnEmailAddressInKey) {
            foreach ($keyValuePairRecords as $index => $record) {
                $this->logger->info('$record',[$record, __METHOD__,__LINE__]);
                $emailAddresses[$record['EMAIL']] = $index;
            }
            $this->logger->info('$emailAddresses', [$emailAddresses, __METHOD__, __LINE__]);
        } else {
            foreach ($keyValuePairRecords as $record) {
                $this->logger->info('$record XXX',[json_encode($record,JSON_PRETTY_PRINT), __METHOD__,__LINE__]);
                $emailAddresses[] = $record['EMAIL'];
            }
            $this->logger->info('$emailAddresses', [$emailAddresses, __METHOD__, __LINE__]);
        }
        return $emailAddresses;
    }


    /**
     * converts $keyValuePairRecords back to csv with field header, ";" as field separator and "\n" as record separator
     * @param array $keyValuePairRecords
     * @return array|null ['header' => $header, 'content' => $content];
     */
    private function getKeyValuePairRecordsAsCsv(array &$keyValuePairRecords):?array
    {
        $header = '';
        $csvRecord = '';
        $content = '';
        $idx=0;

        // early exit
        if(empty($keyValuePairRecords)) {
            return null;
        }

        foreach ($keyValuePairRecords as $record){

            if(0 === $idx){ // get the header and the first content record

                foreach($record as $headerfield => $field) {
                    $header .= $headerfield.';';
                    $csvRecord .= $field.';';
                }

                $header = rtrim($header, ';') . self::LINE_SEPARATOR_N;
                $csvRecord = rtrim($csvRecord, ';') . self::LINE_SEPARATOR_N;

            } else { // get all further content records

                $csvRecord = '';

                foreach($record as $field) {
                    $csvRecord .= $field.';';
                }
                $csvRecord = rtrim($csvRecord, ';') . self::LINE_SEPARATOR_N;
            }

            $content .= $csvRecord;
            $idx++;
        }

        if ($_ENV['APP_ENV'] === 'dev') {
            $date = date('Y-m-d-H-i-');
            file_put_contents(__DIR__ . "/../../var/log/{$date}last-generated.csv", $header . $content);
        }

        return ['header' => $header, 'content' => $content];
    }


    /**
     * distribute to MAILEON ONLY =><br><b style="color:blue">Todo:</b> add factory-method EspServiceFactory::createEspService(new VersandsystemType($versandsystem_type##  )); to treat other systems, ##see $this->allActiveEmailDistributionSystems[1..n]['versandsystem_type']
     * - <b>uses</b> App\Services\CsvProcessingService::ftpToEsp
     * @param string $csvContent
     * @param string $fileName
     * @param bool $allSuccessful (optional), is set to false if one ftp-upload failed
     * @return array
     */
    private function distributeCsvToEsp(string $csvContent, string $fileName, bool &$allSuccessful=true):?array
    {
    /*
        [
            {"versandsystem_type":"Maileon","mandant_id":"6","versandsystem_id":"25","versandsystem_asp":"fulle.media Basis","versandsystem_asp_abkz":"FMB","versandsystem_ftpuser":"RlRQQ29udGFjdHNJbXBvcnRlcjY5ODM=","versandsystem_ftppw":"YnRvMmRTekliNUNR"},
            {"versandsystem_type":"Maileon","mandant_id":"6","versandsystem_id":"26","versandsystem_asp":"fulle.media 2019","versandsystem_asp_abkz":"FM19","versandsystem_ftpuser":"RlRQQ29udGFjdHNJbXBvcnRlcjY5ODQ=","versandsystem_ftppw":"TEdSU3RqbHpzeVBK"},
            {"versandsystem_type":"Maileon","mandant_id":"6","versandsystem_id":"28","versandsystem_asp":"Stellar MDM","versandsystem_asp_abkz":"MDM","versandsystem_ftpuser":"RlRQQ29udGFjdHNJbXBvcnRlcjU2MjQ=","versandsystem_ftppw":"SGw1d0JXRDZIYWlv"},
            {"versandsystem_type":"Maileon","mandant_id":"6","versandsystem_id":"31","versandsystem_asp":"fulle.media Extra","versandsystem_asp_abkz":"FME","versandsystem_ftpuser":"RlRQQ29udGFjdHNJbXBvcnRlcjY5ODY=","versandsystem_ftppw":"UFpnZWtVM2FmOGtG"},
            {"versandsystem_type":"Maileon","mandant_id":"6","versandsystem_id":"144","versandsystem_asp":"FM2021","versandsystem_asp_abkz":"FM21","versandsystem_ftpuser":"RlRQQ29udGFjdHNJbXBvcnRlcjkxODA=","versandsystem_ftppw":"dVZXeWdMMDVhSm9z"},
            {"versandsystem_type":"Maileon","mandant_id":"6","versandsystem_id":"145","versandsystem_asp":"FMReserve","versandsystem_asp_abkz":"FMRV","versandsystem_ftpuser":"RlRQQ29udGFjdHNJbXBvcnRlcjkxODM=","versandsystem_ftppw":"UE5ueHVWVW1oWmsw"},
            {"versandsystem_type":"Maileon","mandant_id":"6","versandsystem_id":"146","versandsystem_asp":"FMService","versandsystem_asp_abkz":"FMSV","versandsystem_ftpuser":"RlRQQ29udGFjdHNJbXBvcnRlcjkyMDE=","versandsystem_ftppw":"TjZReWtoMTR5d3N1"}
        ]
    */
        if(empty($csvContent) || empty($fileName)) {
            $msg = empty($csvContent) ? '$csvContent, ' :'';
            $msg .= empty($fileName) ? '$fileName' :'';
            throw new \Exception('Missing parameters in '.__METHOD__.', '.__LINE__);
        }

        $distributionStatus = [];
        $path = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'var'.DIRECTORY_SEPARATOR.'log'.DIRECTORY_SEPARATOR;
        $this->logger->info('$path.$filename',[$path.$fileName, __METHOD__, __LINE__]);


        $filePutContents = file_put_contents($path.$fileName, $csvContent); // write file temporary to disk for $this->ftpToEsp
        $this->logger->info('$filePutContents',[$filePutContents, __METHOD__, __LINE__]);
        $t1 = microtime(true);


        $this->logger->info('distributing to n systems', [count($this->allActiveEmailDistributionSystems), __METHOD__, __LINE__]);
        file_put_contents($path.'distributingTo.json', json_encode($this->allActiveEmailDistributionSystems, JSON_PRETTY_PRINT));

        foreach ($this->allActiveEmailDistributionSystems as $esp) {

            $this->logger->info("distribution start for {$path}{$fileName}:  ftpUser={$esp['versandsystem_ftpuser']} ", ['espType' => $esp['versandsystem_type'], 'espName' => $esp['versandsystem_asp'], 'espAbkz' =>  $esp['versandsystem_asp_abkz'], __METHOD__, __LINE__]);

            $ftpStatus = $this->ftpToEsp($esp['versandsystem_ftpuser'], $esp['versandsystem_ftppw'], $path.$fileName, $fileName, 'ftp.maileon.com' );

            $this->logger->info("distribution status for:  ftpUser={$esp['versandsystem_ftpuser']} ", [$ftpStatus, __METHOD__, __FILE__]);

            switch ($ftpStatus) {
                case self::FTP_PASV_FAILED:
                    $statusMessage = 'FTP PASV FAILED ';
                    $boolStatus = false;
                    break;
                case self::FTP_UPLOAD_FAILED:
                    $statusMessage = 'FTP UPLOAD FAILED';
                    $boolStatus = false;
                    break;
                case self::FTP_LOGIN_FAILED:
                    $statusMessage = 'FTP LOGIN FAILED';
                    $boolStatus = false;
                    break;
                case self::FTP_SSL_CONNECT_FAILED:
                    $statusMessage = 'FTP SSL CONNECT FAILED';
                    $boolStatus = false;
                    break;
                case self::FTP_UPLOAD_SUCCESSFULL:
                    $statusMessage = 'FTP UPLOAD SUCCESSFULL';
                    $boolStatus = true;
                    break;
                default :
                    $statusMessage = 'No Status Message received';
                    $boolStatus = false;
                    break;
            }

            if($ftpStatus < 1) {
               $this->logger->error("login failed for ftpUser={$esp['versandsystem_ftpuser']} ", ['espType' => $esp['versandsystem_type'], 'espName' => $esp['versandsystem_asp'], 'espAbkz' =>  $esp['versandsystem_asp_abkz'], __METHOD__, __FILE__]);
               $allSuccessful = false; // set optional call by reference parameter
            }

            $t2 = microtime(true);
            $distributionStatus[$esp['mandant_id']][$esp['versandsystem_id']] =  ['status' => $boolStatus, 'message' => $statusMessage, 'espType' => $esp['versandsystem_type'], 'espName' => $esp['versandsystem_asp'], 'espAbkz' =>  $esp['versandsystem_asp_abkz'], 'duration[s]' => ($t2-$t1)];

            // unlink($path.$fileName); // remove temporary file from disk
        }


        return $distributionStatus;
    }



    /**
     * Todo: implement this in \App\EspServices\MaileonService::sendToEsp()
     * @param string $ftpUser
     * @param string $ftpPassword
     * @param string $sourceFile
     * @param string $filename
     * @param string $ftpDomain
     * @return int
     */
    private function ftpToEsp(string $ftpUser, string $ftpPassword, string $csvSourceFile, string $filename, string $ftpDomain = 'ftp.maileon.com'): int
    {
        $this->logger->info('PARAMS', [$ftpUser, $ftpPassword, $csvSourceFile, $filename, $ftpDomain, __METHOD__, __LINE__]);
        $status = -99;
        try {
            $ftpSslConnect = ftp_ssl_connect($ftpDomain);


            if (false === $ftpSslConnect) {
                $this->logger->info('FTP STATUS', [self::FTP_SSL_CONNECT_FAILED, $ftpUser, $ftpPassword, $csvSourceFile, $filename, __METHOD__, __LINE__]);
                return self::FTP_SSL_CONNECT_FAILED;
            }


            if (false === ftp_login($ftpSslConnect, $ftpUser, $ftpPassword)) {
                $this->logger->info('FTP STATUS', [self::FTP_LOGIN_FAILED, $ftpUser, $ftpPassword, $csvSourceFile, $filename, __METHOD__, __LINE__]);
                return self::FTP_LOGIN_FAILED;
            }


            if (false === ftp_pasv($ftpSslConnect, true)) {
                $this->logger->info('FTP STATUS', [self::FTP_PASV_FAILED, $ftpUser, $ftpPassword, $csvSourceFile, $filename, __METHOD__, __LINE__]);
                return self::FTP_PASV_FAILED;
            }


            if (false === ftp_put($ftpSslConnect, 'contacts/' . $filename, $csvSourceFile, FTP_ASCII)) {
                $this->logger->info('FTP STATUS', [self::FTP_UPLOAD_FAILED, $ftpUser, $ftpPassword, $csvSourceFile, $filename, __METHOD__, __LINE__]);
                return self::FTP_UPLOAD_FAILED;
            }

            ftp_close($ftpSslConnect);
            $this->logger->info('FTP STATUS', [self::FTP_UPLOAD_SUCCESSFULL, $ftpUser, $ftpPassword, $csvSourceFile, $filename, __METHOD__, __LINE__]);
            return self::FTP_UPLOAD_SUCCESSFULL;

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [$status, __METHOD__, __LINE__]);
            return $status;
        }

        $this->logger->info('FTP STATUS', [$status, $ftpUser, $ftpPassword, $csvSourceFile, $filename, __METHOD__, __LINE__]);
        return $status;
    }


    /**
     * updates statistics in table import_datei
     * @param int    $mandantId
     * @param string $csvFileName
     * @param array  $statusDistribution => goes to <mandantdb>.import_datei.HINWEIS
     * @param int    $netto
     * @return array
     * @throws \Exception
     */
    private function setFileStatistics(int $mandantId, string $csvFileName, string $transaction_id, int $netto,  array $statusDistribution=[]):array
    {
        $loop=0;
        $processStatus = new ProcessingStatusService();

        // get out one status
        foreach ($this->processStatus as $mandantId => $versandsystem) {
            foreach ($versandsystem as $versandsystemId => $processingStatusService) {
                if (0 === $loop) {
                    $processStatus = $processingStatusService;
                }
                $loop++;
                break; // exit when the 1st one is found, since values of all instances are the same
            }
        }
        $this->logger->info('$processStatus', [$processStatus->getArray(), __METHOD__, __LINE__]);

        $updateStatus = $this->peppmtRepository->updateTableImportDatei($this->cmDbUser, $this->cmDbPassword, $mandantId, $csvFileName, $processStatus,  $transaction_id, $netto, $statusDistribution);

        $this->logger->info('$updateStatus', [$updateStatus, __METHOD__,__LINE__]);
        return $updateStatus;

    }


    /**
     * @param int    $mandantId
     * @param string $fileName
     * @return int|null  mumber of records found in import_datei of $mandantId for $fileName or null if something went wrong
     */
    public function fileNameExists(int $mandantId, string $fileName):?int
    {
        try {
            $fileNameExists = $this->peppmtRepository->fileNameExists($this->cmDbUser, $this->cmDbPassword, $mandantId, $fileName);
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $fileNameExists;
    }

}
