<?php

namespace App\Repository;

use App\Services\ProcessingStatusService;
use Doctrine\DBAL\Driver\Connection as DriverConnection;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Exception;
use Psr\Log\LoggerInterface;

use function Symfony\Component\DependencyInjection\Loader\Configurator\env;

class PeppmtRepository
{

    private Connection $peppmt;
    private LoggerInterface $logger;

    /**
     * @var string|array|false
     */
    private string $CmServerIpAddress;


    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->peppmt = $connection;
        $this->logger = $logger;
        $this->CmServerIpAddress= $_ENV['CM_SERVER_IP'];

        if(false===$this->CmServerIpAddress||empty($this->CmServerIpAddress)) {
            $message = 'Missing CM_SERVER_IP configuration in .env';
            $this->logger->error( $message, [ __METHOD__, __LINE__]);
            throw new \Exception($message . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }
        $logger->info('$this->CmServerIpAddress', [$this->CmServerIpAddress, __METHOD__, __LINE__]);
    }


    /**
     * @param int    $mandant_id
     * @param string $mandant_name
     * @param string $mandant_abkz
     * @return array|null [id,mandant,package,abkz,unsubkey,dm_rep_mail,dm_rep_betreff,untermandant,type,db_user,db_pw,db_name,distribution_list_mandant_ids,distribution_list_delivery_id_sequence_name,active]
     * @throws Exception
     * @internal get a peppmt.mandant record by either mandant_id or mandant_name or mandant_abkz
     */
    public function getMandantRecord(int $mandant_id = 0, string $mandant_name = "", string $mandant_abkz = ""): ?array
    {
        $selectMandant = "SELECT id,mandant,package,abkz,unsubkey,dm_rep_mail,dm_rep_betreff,untermandant,type,db_user,db_pw,db_name,distribution_list_mandant_ids,distribution_list_delivery_id_sequence_name,active FROM  mandant WHERE (id=:mandant_id OR mandant=:mandant_name OR  abkz=:mandant_abkz) AND active=1";
        $stmt = $this->peppmt->prepare($selectMandant);
        $stmt->bindParam(':mandant_id', $mandant_id, \PDO::PARAM_STR);
        $stmt->bindParam(':mandant_name', $mandant_name, \PDO::PARAM_STR);
        $stmt->bindParam(':mandant_abkz', $mandant_abkz, \PDO::PARAM_STR);

        try {
            $result = $stmt->executeQuery();
            $mandantRecord = $result->fetchAllAssociative()[0];
            $this->logger->info('$mandantRecord', [$mandantRecord, __METHOD__, __LINE__]);
            if ($result->rowCount() >= 1) {
                return $mandantRecord;
            }
        } catch (Exception $e) {
            $this->logger->error('sql exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }
        return null;
    }


    /**
     * returns [mandant_id, versandsystem_id, versandsystem_type, m_login, api_user, pw, secret_key] or null
     * @return array|null
     */
    public function getEspConnection(int $mandant_id, int $versandsystem_id): ?array
    {
        $selectEspConnection = "SELECT 
            m.id as mandant_id, 
            v.id as versandsystem_id, 
            v.type as versandsystem_type,
            ma.m_login, 
            ma.api_user, 
            ma.pw, 
            ma.secret_key 
        FROM mandant m
        JOIN mandant_asp ma ON ma.m_id = m.id AND ma.active=1 AND ma.asp_id =:versandsystem_id
        JOIN versandsystem v ON v.id = ma.asp_id AND v.active=1  
        WHERE m.id=:mandant_id 
        AND m.active = 1";

        $stmt = $this->peppmt->prepare($selectEspConnection);
        $stmt->bindParam(':mandant_id', $mandant_id, \PDO::PARAM_STR);
        $stmt->bindParam(':versandsystem_id', $versandsystem_id, \PDO::PARAM_STR);

        try {
            $result = $stmt->executeQuery();
            $espConnectionRecord = $result->fetchAllAssociative()[0];
            $this->logger->info('$espConnectionRecord', [$espConnectionRecord, __METHOD__, __LINE__]);

            if ($result->rowCount() >=1 && !empty($espConnectionRecord)) {
                return $espConnectionRecord;
            }
        } catch (Exception $e) {
            $this->logger->error('sql exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }
        return null;
    }


    /**
     * SELECT v.type as versandsystem_type, m.id as mandant_id , v.id as versandsystem_id, v.asp as versandsystem_asp , v.asp_abkz as versandsystem_asp_abkz, v.ftpuser as versandsystem_ftpuser, v.ftppw as versandsystem_ftppw<br>
     * FROM  mandant m<br>
     * JOIN mandant_asp ma ON ma.m_id = m.id AND ma.active=1<br>
     * JOIN versandsystem v ON v.id = ma.asp_id AND active= 1 AND v.type = :versandsystem_type<br>
     * WHERE m.id in ($distribution_list_mandant_ids) AND m.active=1<br>
     * ORDER BY v.type, m.id, v.id</pre>
     * @param string $distribution_list_mandant_ids
     * @return array|null  [versandsystem_type, mandant_id, versandsystem_id, versandsystem_asp, versandsystem_asp_abkz, versandsystem_ftpuser, versandsystem_ftppw]
     * @throws Exception
     * @internal currently (2022-01) only Maileon records are returned<br><br><pre>
     */
    public function getAllActiveEmailDistributionSystems(string $distribution_list_mandant_ids, $versandsystem_type='Maileon'): ?array
    {
        $select = "SELECT v.type as versandsystem_type, m.id as mandant_id , v.id as versandsystem_id, v.asp as versandsystem_asp , v.asp_abkz as versandsystem_asp_abkz, v.ftpuser as versandsystem_ftpuser, v.ftppw as versandsystem_ftppw   
            FROM  mandant m
            JOIN mandant_asp ma ON ma.m_id = m.id AND ma.active=1
            JOIN versandsystem v ON v.id = ma.asp_id AND v.active= 1 AND v.type = :versandsystem_type
            WHERE m.id in ($distribution_list_mandant_ids) AND m.active=1
            ORDER BY v.type, m.id, v.id";
        $stmt = $this->peppmt->prepare($select);
        $stmt->bindParam(':versandsystem_type', $versandsystem_type, \PDO::PARAM_STR);

        try {
            $result = $stmt->executeQuery();
            $records = $result->fetchAllAssociative();
            if ($result->rowCount() > 0) {
                return $records;
            }
        } catch (Exception $e) {
            $this->logger->error('sql exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage());
        }

        return null;
    }


    /**
     * @param int    $mandant_id
     * @param string $mandant_name
     * @param string $mandant_abkz
     * @return array|null [<b>'conn'</b> => Doctrine\DBAL\Connection|null, <b>'dbname'</b> => dbname]
     * @throws \Doctrine\DBAL\Driver\Exception
     * @internal get a Doctrine\DBAL\Connection along with the database name returned in an array or null in case of error
     * @uses     App\Repository\PeppmtRepository::getMandantRecord
     */
    public function getDbConnectionByMandant(
        string $cmDbUser,
        string $cmDbPassword,
        int $mandant_id = 0,
        string $mandant_name = "",
        string $mandant_abkz = ""
    ): ?array {
        $message = '';
        // $this->logger->info('PARAMS', [$cmDbUser, $cmDbPassword, $mandant_id, $mandant_name, $mandant_abkz, __METHOD__, __LINE__]);

        try {
            $espConnectionRecord = $this->getMandantRecord($mandant_id, $mandant_name, $mandant_abkz);

            if (null === $espConnectionRecord || empty($espConnectionRecord['db_name'])) {
                $message .= (null === $espConnectionRecord) ? 'peppmt.mandant record not found, ' : '';
                $message .= (empty($espConnectionRecord['db_name'])) ? 'peppmt.mandant.db_name not set' : '';
                $this->logger->error($message, [__METHOD__, __LINE__]);
                return null; // exit
            }

            $dbname = base64_decode($espConnectionRecord['db_name']);
            $this->logger->info('$dbname', [$dbname, __METHOD__, __LINE__]);

            $connectionParams = array(
                'dbname' => $dbname,
                'user' => $cmDbUser,
                'password' => $cmDbPassword,
                'host' => $this->CmServerIpAddress,
                'driver' => 'pdo_mysql',
            );
            $this->logger->info('connectionParams', [json_encode($connectionParams), __METHOD__, __LINE__]);
            $t1 = microtime(true);
            $conn = DriverManager::getConnection($connectionParams, new Configuration());
            $t2 = microtime(true);
            $this->logger->info('$conn time', [($t2-$t1), $conn->getDatabase(), __METHOD__, __LINE__]);
        } catch (\Exception $e) {
            $this->logger->error('sql exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception('message='.$e->getMessage().', method='.__METHOD__.', line='.__LINE__);
        }
        return ['conn' => $conn, 'dbname' => $dbname];
    }


    /**
     * get all whitelist domains
     * @return array|null numeric array of whitelist domains
     * @throws Exception
     */
    public function getDomainWhitelist(): ?array
    {
        $return = [];
        $selectDomainWhitelist = "SELECT domain FROM domain_whitelist";
        $stmt = $this->peppmt->prepare($selectDomainWhitelist);
        try {
            $result = $stmt->executeQuery();
            $records = $result->fetchAllAssociative();

            if ($result->rowCount() > 0) {
                $this->logger->info('$records', [$records, __METHOD__, __LINE__]);
                foreach ($records as $record) {
                    $return[] = $record['domain'];
                }
                return $return;
            }
        } catch (Exception $e) {
            $this->logger->error('sql exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage());
        }
        return null;
    }


    /**
     * returns (bool) true if email blacklisted, else (bool) false if email not in record set of blacklist.Blacklist or blacklist.Blacklist_2
     * @param string $emailAddress
     * @param string $blacklistTable
     * @return bool
     * @throws Exception
     */
    public function isBlacklisted(string $emailAddress, string $blacklistTable):bool
    {
        $return = [];
        $selectEmail = "SELECT Email FROM blacklist.{$blacklistTable} WHERE Email=:emailAddress";

        $stmt = $this->peppmt->prepare($selectEmail);
        $stmt->bindParam(':emailAddress', $emailAddress, \PDO::PARAM_STR);
        try {
            $result = $stmt->executeQuery();
            $email = $result->fetchOne();

            if ($result->rowCount() >= 1) {
                $this->logger->info('$email is blacklisted', [$email, __METHOD__, __LINE__]);
                return true;
            }
        } catch (Exception $e) {
            $this->logger->error('sql exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage());
        }
        return false;
    }


    /**
     * @param int $mandant_id
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @internal example for mandant db access to table of choice
     */
    private function exampleMandantDbAccess(int $mandant_id): array
    {
        try {
            $dbConnectionByMandant = $this->peppmtRepository->getDbConnectionByMandant($this->cmDbUser, $this->cmDbPassword, $mandant_id);

            if (null !== $dbConnectionByMandant) {
                $dbConnectionByMandant['conn']->connect();
                $table = $dbConnectionByMandant['dbname'] . '.kampagnen';
                $stmt = $dbConnectionByMandant['conn']->prepare("select versandsystem, k_id, bearbeiter,k_name from {$table} where k_id > 2 LIMIT 3");
                $result = $stmt->executeQuery();
                $records = $result->fetchAllAssociative();

                $this->logger->info('$table', [$table, __METHOD__, __LINE__]);
                $this->logger->info('$records', ['cnt' => $result->rowCount(), '$records' => $records, true, __METHOD__, __LINE__]);
            }
        } catch (Exception $e) {
            $dbConnectionByMandant['conn']->close();
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
        $dbConnectionByMandant['conn']->close();
        return ['status' => true, 'records' => $records];
    }


    /**
     * @param string $cmDbUser
     * @param string $cmDbPassword
     * @param int    $mandantId
     * @param string $emailAddress
     * @return bool
     * @throws \Exception
     */
    public function isExistingExistingLeadsInHistory(string $cmDbUser, string $cmDbPassword, int $mandantId, string $emailAddress, string $csvFilename):bool
    {
        try {
            $dbConnectionByMandant = $this->getDbConnectionByMandant($cmDbUser, $cmDbPassword, $mandantId);
            $dbConnectionByMandant['conn']->connect();

            // get max import nr of csv-file
            $importTable = $dbConnectionByMandant['dbname'] . '.import_datei';
            $stmt = $dbConnectionByMandant['conn']->prepare("SELECT MAX(IMPORT_NR) IMPORT_NR FROM {$importTable} WHERE LOWER(IMPORT_DATEI) = :csvFilename "); // ensure it's the lastest import
            $lCsvFilename = strtolower($csvFilename);
            $stmt->bindParam(':csvFilename', $lCsvFilename, \PDO::PARAM_STR);
            $result = $stmt->executeQuery();
            $importNr = $result->fetchOne();
            $this->logger->info('$importNr', [$importNr, __METHOD__, __LINE__]);

            if (null !== $dbConnectionByMandant) {
                $table = $dbConnectionByMandant['dbname'] . '.history';
                $stmt = $dbConnectionByMandant['conn']->prepare("SELECT EMAIL FROM {$table} WHERE EMAIL = :emailAddress AND IMPORT_NR < :import_nr"); // get only emails of previous uploads (CM inserts email records during file import due to step2.php-step5.php!)
                $stmt->bindParam(':emailAddress', $emailAddress, \PDO::PARAM_STR);
                $stmt->bindParam(':import_nr', $importNr, \PDO::PARAM_INT);
                $result = $stmt->executeQuery();
                $records = $result->fetchOne();

                $this->logger->info('$table', [$table, __METHOD__, __LINE__]);
                $this->logger->info('$records', ['cnt' => $result->rowCount(), '$records' => $records, __METHOD__, __LINE__]);

                if($result->rowCount() >= 1) {
                    $dbConnectionByMandant['conn']->close();
                    return true;
                }
            }
        } catch (Exception $e) {
            $dbConnectionByMandant['conn']->close();
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception('message='.$e->getMessage().', method='.__METHOD__.', line='.__LINE__);
        }
        $dbConnectionByMandant['conn']->close();
        return false;
    }


    /**
     * @param string $cmDbUser
     * @param string $cmDbPassword
     * @param int    $mandantId
     * @param string $emailAddress
     * @return bool
     * @throws \Exception
     */
    public function isUnsubscribed(string $cmDbUser, string $cmDbPassword, int $mandantId, string $emailAddress): bool
    {
        $table = 'abmelder_ems.mandant_'.$mandantId;
        $t1 = microtime(true);
        try {
            $stmt = $this->peppmt->prepare("SELECT 1 FROM {$table} WHERE email = :emailAddress");
            $stmt->bindParam(':emailAddress', $emailAddress,\PDO::PARAM_STR);
            $result = $stmt->executeQuery();
            $records = $result->fetchOne();


            $this->logger->info('$table', [$table, __METHOD__, __LINE__]);
            $this->logger->info('$records', ['cnt' => $result->rowCount(), '$records' => $records, true, __METHOD__, __LINE__]);

            if ($result->rowCount() >= 1) {
                $this->logger->info("UNSUB: $emailAddress is unsubscribed", [__METHOD__, __LINE__]);

                return true;
            }
            $t2 = microtime(true);
            $this->logger->info("TIME FOR GETTING UNSUB FROM TAB {$table}", [($t2-$t1), __METHOD__, __LINE__] );

        } catch (Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception('message=' . $e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }
        return false;
    }


    /**
     * @param string                  $cmDbUser
     * @param string                  $cmDbPassword
     * @param int                     $mandantId
     * @param string                  $csvFilename
     * @param ProcessingStatusService $processStatus
     * @param int                     $netto
     * @param array                   $statusDistribution
     * @return array|bool
     */
    public function updateTableImportDatei(string $cmDbUser, string $cmDbPassword, int $mandantId, string $csvFilename, ProcessingStatusService $processStatus, string $transaction_id, int $netto, array $statusDistribution=[]):array
    {
        $tlCsvFileName = trim(strtolower($csvFilename));
        $this->logger->info('$csvFileName', [$tlCsvFileName, $csvFilename, __METHOD__, __LINE__]);

        try {
            $dbConnectionByMandant = $this->getDbConnectionByMandant($cmDbUser, $cmDbPassword, $mandantId);
            $importDatum = date('Y-m-d H:i:s');

            if (null !== $dbConnectionByMandant) {
                $dbConnectionByMandant['conn']->connect();
                $table = $dbConnectionByMandant['dbname'] . '.import_datei';
                // replace {$table} with leadworld_maas_db.import_datei  to get IDE support for SQL statement
                $stmt = $dbConnectionByMandant['conn']->prepare(
                    "UPDATE {$table} t SET  
                        t.BRUTTO=:brutto, 
                        t.NETTO=:netto, 
                        t.METADB=:abmelderHits,
                        t.DUBLETTE_INDATEI=:dubletteInDatei,
                        t.DUBLETTEN=:dubletten,
                        t.BLACKLIST=:blacklist,
                        t.FAKE=:incompleteRecords,
                        t.IMPORT_DATUM=:importDatum,
                        t.HINWEIS=:hinweis,
                        t.STATUS=200
                    WHERE trim(lower(t.IMPORT_DATEI))=:csvFilename");  // t.STATUS=200  displays 'Vorverarbeitet<br/>FTP Upload' in CM Menu "Importe" for corresponding mandant

                $stats = $this->calculateProcessStatusIntoStatisticsArray($processStatus, $netto, $csvFilename);

                if(!empty($statusDistribution)) {
                    $hinweis = json_encode(['processStatus' => $stats, 'statusDistribution' => $statusDistribution]);
                } else {
                    $hinweis = '';
                }

                $stmt->bindParam(':brutto', $stats['import_datei']['brutto'], \PDO::PARAM_INT);
                $stmt->bindParam(':netto', $stats['import_datei']['netto'], \PDO::PARAM_INT);
                $stmt->bindParam(':abmelderHits', $stats['import_datei']['abmelderHits'], \PDO::PARAM_INT);
                $stmt->bindParam(':dubletteInDatei', $stats['import_datei']['dubletteInDatei'], \PDO::PARAM_INT);
                $stmt->bindParam(':dubletten', $stats['import_datei']['dubletten'], \PDO::PARAM_INT);
                $stmt->bindParam(':blacklist', $stats['import_datei']['blacklist'], \PDO::PARAM_INT);
                $stmt->bindParam(':incompleteRecords', $stats['import_datei']['incompleteRecords'], \PDO::PARAM_INT); // in reality count of incomplete records

                $stmt->bindParam(':importDatum', $importDatum, \PDO::PARAM_STR);
                $stmt->bindParam(':hinweis', $hinweis, \PDO::PARAM_STR);
                $stmt->bindParam(':csvFilename', $tlCsvFileName, \PDO::PARAM_STR);

                $result = $stmt->executeQuery();

                $this->logger->info('$table', [$table, __METHOD__, __LINE__]);
                $this->logger->info('$records', ['cnt' => $result->rowCount(), __METHOD__, __LINE__]);

                if ($result->rowCount() >= 1) {
                    try {
                        $stmtUpdate = $this->peppmt->prepare(
                            'UPDATE ftp_upload_planer SET 
                                    processing_status=:processing_status, 
                                    preprocessing_stats=:hinweis, 
                                    preprocessed_at=:preprocessed_at 
                                WHERE mandant_id=:mandant_id 
                                AND transaction_id=:transaction_id
                                AND filename = :csvfilename');
                        $processing_status='preprocessed';
                        $stmtUpdate->bindParam(':processing_status', $processing_status);
                        $stmtUpdate->bindParam(':hinweis', $hinweis);
                        $preprocessed_at = date('Y-m-d H:i:s');
                        $stmtUpdate->bindParam(':preprocessed_at', $preprocessed_at);
                        $stmtUpdate->bindParam(':mandant_id', $mandantId, \PDO::PARAM_INT);
                        $stmtUpdate->bindParam(':transaction_id', $transaction_id);
                        $stmtUpdate->bindParam(':csvfilename', $csvFilename);
                        $stmtUpdate->execute();// fire and forget

                    } catch (Exception $e) {
                        $this->logger->error("SQL ERROR", [$e->getMessage(), __METHOD__, __LINE__]);
                    }

                    return  ['status' => true, 'message' => "successful update of $table with statistics", 'statistics' => $stats ];
                }
            }
        } catch (Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            return ['status' => false, 'message' => "error updating $table with statistics. Exception: ". $e->getMessage(), 'statistics' => $stats,  'method' => __FUNCTION__, 'line' => __LINE__ ];
        }
        return ['status' => false, 'message' => "error updating $table with statistics", 'statistics' => $stats ];
    }


    /**
     * @param string $cmDbUser
     * @param string $cmDbPassword
     * @param int    $mandantId
     * @param array  $allCsvFilenames string[]
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function updateTableImportDateiStatusToUploaded(string $cmDbUser, string $cmDbPassword, int $mandantId, array $allCsvFilenames)
    {
        try {
            $dbConnectionByMandant = $this->getDbConnectionByMandant($cmDbUser, $cmDbPassword, $mandantId);
            $importDatum = date('Y-m-d H:i:s');

            if (null !== $dbConnectionByMandant) {
                $dbConnectionByMandant['conn']->connect();
                $table = $dbConnectionByMandant['dbname'] . '.import_datei';

                $updatedRows = 0;
                foreach ($allCsvFilenames as $filename) {
                    $tlCsvFileName = trim(strtolower($filename));

                    $stmt = $dbConnectionByMandant['conn']->prepare(
                        "UPDATE {$table} t SET  
                            t.STATUS=300
                        WHERE trim(lower(t.IMPORT_DATEI))=:csvFilename");// t.STATUS=300  displays 'Uploaded<br/>FTP Upload' in CM Menu "Importe" for corresponding mandant

                    $stmt->bindParam(':csvFilename', $tlCsvFileName, \PDO::PARAM_STR);

                    $result = $stmt->executeQuery();
                    $updatedRows +=$result->rowCount();

                    $this->logger->info('$table', [$table, __METHOD__, __LINE__]);
                    $this->logger->info('$records', ['cnt' => $result->rowCount(), __METHOD__, __LINE__]);
                }
                $this->logger->info("update {$table}", ['updatedRows'=> $updatedRows, __METHOD__, __LINE__]);
                $dbConnectionByMandant['conn']->close();
                return  ['status' => true, 'message' => "successful updated {$updatedRows} rows of $table with status 300"];
            }
        } catch (Exception $e) {
            $dbConnectionByMandant['conn']->close();
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            return ['status' => false, 'message' => "error updating $table with statistics. Exception: ". $e->getMessage(), 'method' => __FUNCTION__, 'line' => __LINE__ ];
        }
        $dbConnectionByMandant['conn']->close();
        return ['status' => false, 'message' => "error updating $table with statistics"];

    }


    /**
     * @param ProcessingStatusService $processStatus
     * @param int                     $netto
     * @return array[]
     */
    private function calculateProcessStatusIntoStatisticsArray(ProcessingStatusService $processStatus, int $netto, string $csvFilename)
    {
        $processStatusArray = [
            'import_datei' => [
                'brutto' => $processStatus->getCountBrutto(),
                'netto' => $processStatus->getCountBrutto() -
                    (
                        $processStatus->getCountBlacklistHits() +
                        $processStatus->getCountNonWhiteListHits() +
                        count($processStatus->getArrayIncompleteRecords()) +
                        $processStatus->getCountExistingLeadsInHistory()+
                        $processStatus->getCountInSetDuplicates()
                    ),
                'dubletteInDatei' => $processStatus->getCountInSetDuplicates(),
                'dubletten' => $processStatus->getCountExistingLeadsInHistory(),
                'blacklist' => $processStatus->getCountBlacklistHits() + $processStatus->getCountNonWhiteListHits(),
                'abmelderHits' => $processStatus->getCountAbmelderHits(), // andere Bedeutung in UI !!!
                'incompleteRecords' => count($processStatus->getArrayIncompleteRecords()), // andere Bedeutung in UI
            ],
            'single_values' => [
                'hinweis' => 'hier kann es Ueberschneidungen geben',
                'CountBlacklistHits' => $processStatus->getCountBlacklistHits(),
                'CountNonWhiteListHits' => $processStatus->getCountNonWhiteListHits(),
                'CountIncompleteRecords' => count($processStatus->getArrayIncompleteRecords()),
                'CountExistingLeadsInHistory' => $processStatus->getCountExistingLeadsInHistory(),
                'CountAbmelderHits' => $processStatus->getCountAbmelderHits(),
                'CountInSetDuplicates' => $processStatus->getCountInSetDuplicates(),
            ]
        ];

        $this->logger->info('$processStatusArray', [$processStatusArray, __METHOD__, __LINE__]);

        try {
            file_put_contents(__DIR__ . '/../../var/log/STATS-' . $csvFilename . '.json', json_encode($processStatusArray, JSON_PRETTY_PRINT));
        } catch (\Exception $e) {
            ;
        }

        return $processStatusArray;
    }


    /**
     * @param string $cmDbUser
     * @param string $cmDbPassword
     * @param int    $mandantId
     * @param string $csvFilename
     * @return int|null
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    private function getImportDateiNr(string $cmDbUser, string $cmDbPassword, int $mandantId, string $csvFilename): ?int
    {
        $tlCsvFileName = trim(strtolower($csvFilename));
        try {
            $dbConnectionByMandant = $this->getDbConnectionByMandant($cmDbUser, $cmDbPassword, $mandantId);

            if (null !== $dbConnectionByMandant) {
                $dbConnectionByMandant['conn']->connect();
                $table = $dbConnectionByMandant['dbname'] . '.import_datei';

                $stmt = $dbConnectionByMandant['conn']->prepare("SELECT MAX(IMPORT_NR) FROM {$table} t  WHERE trim(lower(t.IMPORT_DATEI))=:csvFileName");
                $stmt->bindParam(':csvFileName', $tlCsvFileName, \PDO::PARAM_STR);

                $result = $stmt->executeQuery();
                $importNr = $result->fetchOne();

                $this->logger->info('$table', [$table, __METHOD__, __LINE__]);
                $this->logger->info('$records import_datei', ['cnt' => $result->rowCount(), '$records' => $result, '$importNr' => $importNr, __METHOD__, __LINE__]);

                if ($result->rowCount() === 1) {
                    $this->logger->info("import_datei has $csvFilename record", [__METHOD__, __LINE__]);
                    $dbConnectionByMandant['conn']->close();
                    return $importNr;
                }
            }
        } catch (Exception $e) {
            $dbConnectionByMandant['conn']->close();
            $this->logger->error("Exception searching {$table}  for IMPORT_DATEI {$csvFilename}:", [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }
        $dbConnectionByMandant['conn']->close();
        return null;
    }


    /**
     * CAUTION: email in-file-duplicates that where previously inserted by CM into table history will not be removed by this statement, since the original first record of that email address is present in $allEmailAddresses of the distribution file
     * @param string $cmDbUser
     * @param string $cmDbPassword
     * @param int    $mandantId
     * @param array  $allEmailAddresses
     * @param string $csvFilename
     * @return int number of records deleted in <mandantdb>.history
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function removeRecordsFromHistoryTable(string $cmDbUser, string $cmDbPassword, int $mandantId, array $allEmailAddresses, string $csvFilename):int
    {
        $this->logger->info('PARAMS removeRecordsFromHistoryTable', [$cmDbUser,  $cmDbPassword,  $mandantId,  $csvFilename, $allEmailAddresses, __METHOD__, __LINE__]);
        $emailList = '';
        foreach ($allEmailAddresses as $email) {
            $emailList .= "'".$email."',";
        }
        $emailList = rtrim($emailList, ',');
        $this->logger->info('$emailList', [$emailList, __METHOD__, __LINE__]);

        try {
            $importNr = $this->getImportDateiNr($cmDbUser, $cmDbPassword, $mandantId, $csvFilename);
            $this->logger->info('$importNr', [$importNr, __METHOD__, __LINE__]);
            $dbConnectionByMandant = $this->getDbConnectionByMandant($cmDbUser, $cmDbPassword, $mandantId);
            $this->logger->info('$dbConnectionByMandant', [$dbConnectionByMandant, __METHOD__, __LINE__]);

            if (null !== $dbConnectionByMandant && null !== $importNr) {
                $table = $dbConnectionByMandant['dbname'] . '.history';
                $this->logger->info('table', [$table, __METHOD__, __LINE__]);
                $this->logger->info("DELETING RECORDS IN $table FOR IMPORT FILE ($csvFilename) / IMPORT_NR={$importNr} AND EMAILS NE ($emailList)", [__METHOD__, __LINE__]);
                $dbConnectionByMandant['conn']->connect();

                // replace {$table} with leadworld_maas_db.import_datei  to get IDE support for SQL statement
                $stmt = $dbConnectionByMandant['conn']->prepare("DELETE FROM $table WHERE IMPORT_NR=:importNr AND EMAIL NOT IN ({$emailList})");
                $stmt->bindParam(':importNr', $importNr, \PDO::PARAM_INT);
                $result = $stmt->executeQuery();
                $dbConnectionByMandant['conn']->close();
                return $result->rowCount();
            }
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $dbConnectionByMandant['conn']->close();
            $this->logger->error("Exception deleting records in {$table}  for IMPORT_NR {$importNr}:", [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }

        $dbConnectionByMandant['conn']->close();
        $this->logger->warning("could not get connection or delete records in $table. Cannot delete records in table history of mandantId={$mandantId}", ['rowcount' => $result->rowCount(), __METHOD__, __LINE__]);
        return 0;
    }


    /**
     * get <mandantdb>.history.id by EMAIL
     * @param string $cmDbUser
     * @param string $cmDbPassword
     * @param int    $mandantId
     * @param string $emailAddress
     * @return array|null   <pre>[ID, DS_ID, HERKUNFT] | null</pre>
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function getHistoryIdAndDsIdAndHerkunft(string $cmDbUser, string $cmDbPassword, int $mandantId, string $emailAddress, string $csvFilename):?array
    {
        try {

            $dbConnectionByMandant = $this->getDbConnectionByMandant($cmDbUser, $cmDbPassword, $mandantId);

            if (null !== $dbConnectionByMandant) {
                $dbConnectionByMandant['conn']->connect();
                $table = $dbConnectionByMandant['dbname'] . '.history';
                $importNr = $this->getImportDateiNr($cmDbUser, $cmDbPassword, $mandantId, $csvFilename);

                // get the latest record of an email address in history table
                $stmt = $dbConnectionByMandant['conn']->prepare(
                    "SELECT 
                        h.ID,
                        h.DS_ID,
                        h.HERKUNFT
                    FROM {$table} h 
                    WHERE h.EMAIL= :emailAddress
                    AND h.IMPORT_NR = :importNr
                    ORDER BY h.ID DESC
                    LIMIT 1"
                ); // limit 1, because there can be email duplicates entered by CM
                $stmt->bindParam(':emailAddress', $emailAddress, \PDO::PARAM_STR);
                $stmt->bindParam(':importNr', $importNr, \PDO::PARAM_STR);

                $result = $stmt->executeQuery();
                $historyIdDsId = $result->fetchAssociative();

                $this->logger->info('$table', [$table, __METHOD__, __LINE__]);
                $this->logger->info('$records history', ['cnt' => $result->rowCount(), '$historyIdDsId' => $historyIdDsId, true, __METHOD__, __LINE__]);

                if ($result->rowCount() === 1) {
                    $this->logger->info("history has $emailAddress record", [__METHOD__, __LINE__]);
                    $dbConnectionByMandant['conn']->close();
                    return $historyIdDsId;
                }
            }
        } catch (Exception $e) {
            $dbConnectionByMandant['conn']->close();
            $this->logger->error("Exception searching {$table}  for EMAIL {$emailAddress}:", [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }
        $dbConnectionByMandant['conn']->close();
        return null;
    }


    /**
     * @param string $cmDbUser
     * @param string $cmDbPassword
     * @param int    $mandantId
     * @param string $emailAddress
     * @return array|null
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function getPartnerId(string $cmDbUser, string $cmDbPassword, int $mandantId, string $emailAddress, string $csvFilename):?string
    {
        try {

            $dbConnectionByMandant = $this->getDbConnectionByMandant($cmDbUser, $cmDbPassword, $mandantId);

            if (null !== $dbConnectionByMandant) {
                $dbConnectionByMandant['conn']->connect();
                $table = $dbConnectionByMandant['dbname'] . '.history';
                $importNr = $this->getImportDateiNr($cmDbUser, $cmDbPassword, $mandantId, $csvFilename);

                // get the latest records PARTNER (-ID) of an EMAIL, if there are several entries with the same email address
                // SELECT id.PARTNER FROM <mandant-db>.history h JOIN import_datei id on h.IMPORT_NR = id.IMPORT_NR WHERE h.ID = (SELECT MAX(h.ID) FROM history h WHERE h.EMAIL = :emailAddress);
                $stmt = $dbConnectionByMandant['conn']->prepare(
                    "SELECT 
                        id.PARTNER 
                    FROM {$table} h 
                    JOIN import_datei id on h.IMPORT_NR = id.IMPORT_NR 
                    WHERE h.ID = (SELECT MAX(h.ID) FROM history h WHERE h.EMAIL = :emailAddress)
                    AND h.IMPORT_NR = :importNr
                    "
                );
                $stmt->bindParam(':emailAddress', $emailAddress, \PDO::PARAM_STR);
                $stmt->bindParam(':importNr', $importNr, \PDO::PARAM_INT);

                $result = $stmt->executeQuery();
                $partnerId = $result->fetchOne();

                $this->logger->info('$table', [$table, __METHOD__, __LINE__]);
                $this->logger->info('$records history', ['cnt' => $result->rowCount(), '$partnerId' => $partnerId, true, __METHOD__, __LINE__]);

                if ($result->rowCount() === 1) {
                    $this->logger->info("history has {$emailAddress} record with PARTNER $partnerId", [__METHOD__, __LINE__] );
                    $dbConnectionByMandant['conn']->close();
                    return $partnerId;
                }
            }
        } catch (Exception $e) {
            $dbConnectionByMandant['conn']->close();
            $this->logger->error("Exception searching {$table}  by EMAIL {$emailAddress} for PARTNER (ID):", [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }
        $dbConnectionByMandant['conn']->close();
        return '';
    }


    /**
     * @param int    $mandantId
     * @param string $transaction_id
     * @param array  $statusDistribution
     */
    public function setFtpUploadPlanerUploadStatus(int $mandantId, string $transaction_id, string $uploadFileCsvFileName, array $statusDistribution):void
    {
        try {
            $stmtUpdate = $this->peppmt->prepare('UPDATE ftp_upload_planer SET 
                                    processing_status=:processing_status, 
                                    upload_end_datetime=:upload_end_datetime,
                                    uploaded_to_esp=:uploaded_to_esp,
                                    consolidation_filename=:consolidation_filename 
                                WHERE mandant_id=:mandant_id 
                                AND transaction_id=:transaction_id
                                -- AND filename = :csvfilename');
            $processing_status = 'uploaded';
            $stmtUpdate->bindParam(':processing_status', $processing_status);
            $upload_end_datetime = date('Y-m-d H:i:s');
            $stmtUpdate->bindParam(':upload_end_datetime', $upload_end_datetime);
            $uploaded_to_esp = (empty($statusDistribution)) ? '' : json_encode($statusDistribution);
            $stmtUpdate->bindParam(':uploaded_to_esp', $uploaded_to_esp);
            $stmtUpdate->bindParam(':consolidation_filename', $uploadFileCsvFileName);
            $stmtUpdate->bindParam(':mandant_id', $mandantId, \PDO::PARAM_INT);
            $stmtUpdate->bindParam(':transaction_id', $transaction_id);// $stmtUpdate->bindParam(':csvfilename', $csvFileName);
            $stmtUpdate->execute();// fire and forget

        } catch (Exception $e) {
            $this->logger->error("SQL ERROR", [$e->getMessage(), __METHOD__, __LINE__]);
        }

    }


    /**
     * @param string $cmDbUser
     * @param string $cmDbPassword
     * @param int    $mandantId
     * @param string $csvFileName
     * @return int|null
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function fileNameExists(string $cmDbUser, string $cmDbPassword, int $mandantId, string $csvFileName): ?int
    {
        $tlCsvFileName = trim(strtolower($csvFileName));
        try {
            $dbConnectionByMandant = $this->getDbConnectionByMandant($cmDbUser, $cmDbPassword, $mandantId);
            if (null !== $dbConnectionByMandant) {
                $dbConnectionByMandant['conn']->connect();
                $table = $dbConnectionByMandant['dbname'] . '.import_datei';

                $stmt = $dbConnectionByMandant['conn']->prepare("SELECT COUNT(*) FROM {$table} t  WHERE trim(lower(t.IMPORT_DATEI))=:csvFileName");
                $stmt->bindParam(':csvFileName', $tlCsvFileName, \PDO::PARAM_STR);

                $result = $stmt->executeQuery();
                $countFileName = $result->fetchOne();

                $this->logger->info('$table', [$table, __METHOD__, __LINE__]);
                $this->logger->info('$records fileNameExists import_datei', ['file' => $csvFileName, '$countFileName' => $countFileName, __METHOD__, __LINE__]);

                return $countFileName;
            }
        } catch (Exception $e) {
            $dbConnectionByMandant['conn']->close();
            $this->logger->error("fileNameExists Exception searching {$table}  for IMPORT_DATEI {$csvFilename}:", [$e->getMessage(), __METHOD__, __LINE__]);
            throw new \Exception($e->getMessage() . ', method=' . __METHOD__ . ', line=' . __LINE__);
        }
        $dbConnectionByMandant['conn']->close();
        return null;
    }

}
