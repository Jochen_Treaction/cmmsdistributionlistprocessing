<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
// use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use App\Services\SecurityService as AppSecurityService;
use Psr\Log\LoggerInterface;

class RequestListener
{
    public function __construct(AppSecurityService $securityService,LoggerInterface $logger)
    {
        $this->securityService = $securityService;
        $this->logger = $logger;
    }


    private function sendAuthorizationHeader()
    {
        header('HTTP/1.0 401 Unauthorized');
        die();
    }


    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $clientIp = $requestEvent->getRequest()->getClientIp();

        if(!$this->securityService->isAllowedIp($clientIp)) {
            $this->logger->alert('unauthorised access of ip', [$clientIp, __FUNCTION__, __LINE__]);
            $this->sendAuthorizationHeader();
        } else {
            $this->logger->info("allowed access of ip $clientIp", [__FUNCTION__, __LINE__]);
        }
    }
}
