<?php

namespace App\Controller;

use App\Repository\PeppmtRepository;
use Doctrine\DBAL\Driver\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\CsvProcessingService;
use Psr\Log\LoggerInterface;


class CsvDistributionController extends AbstractController
{

    public function __construct(CsvProcessingService $csvHandlerService, LoggerInterface $logger)
    {
        $this->csvHandlerService = $csvHandlerService;
        $this->logger = $logger;
    }


    /**
     * @Route("/preprocess", name="preprocess", methods={"POST"})
     *
     * @internal does a smart distribution of received csv files containing email distribution records ("Verteilerlisten") to (currently only) MAILEON Email Distribution Systems
     *           <br><b>triggred by CM CRON JOB campaign-treaction/cron/FtpUploadByPeppmtFtpUploadPlaner.php</b>
     *           <br>data structure of csv files: <pre>'EMAIL;ID;HERKUNFTID;ANREDE;VORNAME;NACHNAME;STRASSE;PLZ;ORT;LAND;GEBURTSDATUM;PARTNER;DOI_DATE;DS_ID;PUB_ID'</pre>
     */
    public function preProcess(
        Request $request,
        CsvProcessingService $csvHandlerService,
        LoggerInterface $logger
    ) {
        // go on processing the data
        $t1 = microtime(true);

        try {
            $content = json_decode($request->getContent(), true, 512, JSON_OBJECT_AS_ARRAY | JSON_THROW_ON_ERROR);
            $this->logger->info('$content', [$content,__METHOD__,__LINE__]);
            $contentCsv = (!empty($content['csv'])) ? $content['csv'] : null;
            $csvFilename = (!empty($content['csvFilename'])) ? $content['csvFilename'] : null;
            $this->logger->info('csvFilename', [$csvFilename, __METHOD__, __LINE__]);
            $contentMandantId = (!empty($content['mandant_id'])) ? $content['mandant_id'] : null;
            $contentTransactionId = (!empty($content['transaction_id'])) ? $content['transaction_id'] : null;
            $contentUpdateExistingRecords = (isset($content['update_existing_records']) && is_bool($content['update_existing_records'])) ? $content['update_existing_records'] : false; // don't update, if param is not set or not a bool (true || false, but not 0 || 1)
        } catch (\JsonException $e) {
            $this->logger->error('cannot json decode post body', [$e->getMessage(), __METHOD__, __LINE__]);
            $this->json(['status' => false, 'message' => $e->getMessage(), 'method' => __METHOD__, 'line' => __LINE__]);
        }

        if(empty($contentCsv) || empty($csvFilename) || empty($contentMandantId) || empty($contentTransactionId)) {
            $this->logger->error('Missing Parameters', ['contentCsv or csvFilename, contentMandantId or contentTransactionId', __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'Error: Missing Parameters (contentCsv or csvFilename or contentMandantId )' , 'method' => __METHOD__, 'line' => __LINE__]);
        }

        try {
            $csvToArray = $csvHandlerService->preProcessCsv($contentCsv, $csvFilename, (int)$contentMandantId, (string) $contentTransactionId, $contentUpdateExistingRecords);
            $logger->info('$csvToArray', [$csvToArray, __METHOD__, __LINE__]);
        } catch (\Exception $e) {
            return $this->json(['status' => false, 'message' => $e->getMessage(), 'method' => __METHOD__, 'line' => __LINE__]);
        }

        $t2 = microtime(true);

        if(true === $csvToArray['status']) {
            return $this->json([
                'status' => true,
                'message' => 'successfully distributed',
                'totalDuration_s' => ($t2 - $t1),
                'timeForBlacklistCheck' => $csvToArray['timeForBlacklistCheck'],
                'timeForUnsubscribersExistingEmailsCheck' => $csvToArray['timeForUnsubscribersExistingEmailsCheck'],
                'fileStatistics' => $csvToArray['statistics'], // todo: check its not set
            ]);
        }

        // $csvToArray['status'] === false
        return $this->json([
            'status' => false,
            'message' => 'all records where removed by checks, nothing to distribute ... ',
            'totalDuration_s' => ($t2 - $t1),
            'timeForBlacklistCheck' => $csvToArray['timeForBlacklistCheck'],
            'timeForUnsubscribersExistingEmailsCheck' => $csvToArray['timeForUnsubscribersExistingEmailsCheck'],
            'fileStatistics' => $csvToArray['statistics'],
        ]);

    }


    /**
     * @Route("/distributecsv", name="distributecsv", methods={"POST"})
     * @internal distributecsv consolidates and distributes all cleaned up csv files of a mandant_id and transaction_id
     *          <br><b>triggered by CM CRON JOB campaign-treaction/cron/FtpUploadByPeppmtFtpUploadPlaner.php</b>
     */
    public function distributeCsv(Request $request, CsvProcessingService $csvHandlerService, LoggerInterface $logger)
    {
        $t1 = microtime(true);

        try {
            $content = json_decode($request->getContent(), true, 512, JSON_OBJECT_AS_ARRAY | JSON_THROW_ON_ERROR);
            $contentMandantId = (!empty($content['mandant_id'])) ? $content['mandant_id'] : null;
            $contentTransactionId = (!empty($content['transaction_id'])) ? $content['transaction_id'] : null;
            $this->logger->info('$content', [$content,__METHOD__,__LINE__]);
        } catch (\JsonException $e) {
            $logger->error('cannot json decode post body', [$e->getMessage(), __METHOD__, __LINE__]);
            $this->json(['status' => false, 'message' => $e->getMessage(), 'method' => __METHOD__, 'line' => __LINE__]);
        }

        if(empty($contentMandantId) || empty($contentTransactionId)) {
            $logger->error('Missing Parameters', ['contentMandantId or contentTransactionId', __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'Error: Missing Parameters (contentCsv or csvFilename or contentMandantId )' , 'method' => __METHOD__, 'line' => __LINE__]);
        }


        try {
            $statusConsolidateAndFtpUpload = $csvHandlerService->consolidateAndFtpUpload($contentMandantId, $contentTransactionId);
        } catch (\Exception $e) {
            $t2 = microtime(true);
            return $this->json( [
                'status' => false,
                'message' => $e->getMessage(),
                'uploadFileCsvFileName' => '',
                'bruttoRecords' => '',
                'nettoRecords' => '',
                'statusDistribution' => '',
                'duration[s]' => ($t2-$t1),
            ]);
        }

        $t2 = microtime(true);

        return $this->json(array_merge($statusConsolidateAndFtpUpload, ['duration[s]' => ($t2-$t1)]));


    }


    /**
     * @Route("/filename/exists/{mandantId}/{fileName}", name="fileNameExists", methods={"GET"})
     * @param Request $request
     */
    public function fileNameExists(Request $request, CsvProcessingService $csvProcessingService):JsonResponse
    {
        $mandantId = $request->get('mandantId');
        $fileName = $request->get('fileName');
        $count = $csvProcessingService->fileNameExists($mandantId, $fileName);
        return $this->json(['fileName' => $fileName,'count' => $count]);
    }


    /**
     * @Route("/check/alive", name="checkalive", methods={"GET"})
     */
    public function checkAlive(Request $request): Response
    {
        return $this->json([
            'your ip' => $request->getClientIp(),
            'alive' => true,
            'sqid' => $this->csvHandlerService->getSequenceNextValue('sq99'),
            // 'getRedisAllActiveEmailDistributionSystems' => $this->csvHandlerService->getRedisAllActiveEmailDistributionSystems('21a9a1d567144782d717cb40409622df'),
            // 'redis' => $this->csvHandlerService->deleteRedis('c1763b52f298425258af9d2e52eaa3df'),
            // 'redis' => $this->csvHandlerService->redisGetAllFilenames('test'),
        ]);
    }


    /**
     * @internal currently not used in this class, but can be used to obtain parameter values defined in services.yaml
     * @param string $name
     * @return array|bool|float|int|string|null
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function getParameter(string $name)
    {
        return $this->container->get('parameter_bag')->get($name);
    }


}
