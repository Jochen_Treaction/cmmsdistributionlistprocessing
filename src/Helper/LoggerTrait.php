<?php

namespace App\Helper;

use Psr\Log\LoggerInterface;

/**
 * @see https://symfonycasts.com/screencast/symfony4-fundamentals/logger-trait
 */
trait LoggerTrait
{
    /**
     * @var LoggerInterface|null
     */
    private $logger;
    /**
     * @required
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    private function logInfo(string $message, array $context = [])
    {
        if ($this->logger) {
            $this->logger->info($message, $context);
        }
    }

}
